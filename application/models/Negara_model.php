<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Negara_model extends CI_Model{

    private $_table = "master_negara";

    public function getAll(){
        $this->db->order_by("nama_negara asc");
        return $this->db->get($this->_table)->result();
          //Artinya seperti select * from master_level_soal
     }

     public function getById($id){
      $query = $this->db->query("
                                  SELECT *
                                  from master_negara
                                  where master_negara.id_negara = $id;
      ");
      $res = $query->row();
      return $res;
    }

    public function validate_form(){
      $this->form_validation->set_rules('nama_negara','Nama Negara','required');
      $this->form_validation->set_rules('negara_singkatan','Negara Singkatan','required');
      
      $validation = $this->form_validation->run();
      if($validation == TRUE){
        return TRUE;
      }
      else{
        return FALSE;
      }

    }

    public function save(){

      $post = $this->input->post(); 
      $nama_negara = $post["nama_negara"];
      $negara_singkatan = $post["negara_singkatan"];

      $this->db->query('alter table master_negara AUTO_INCREMENT=0');

      $data_negara = $this->getAll();
      $cek = true;

      foreach($data_negara as $i){
        $nama_temp = $i->nama_negara;
        $singkatan_temp = $i->negara_singkatan;

        if($nama_negara == $nama_temp || $negara_singkatan == $singkatan_temp){
            $cek = false;
            break;
        }
      }


      if($cek == true){
          $query = $this->db->query("
                                    INSERT INTO master_negara(nama_negara,negara_singkatan)
                                    VALUES ('$nama_negara','$negara_singkatan');
          ");

          return true;
      }
      else{
          return false;
      }

  }

    public function update(){
      $post = $this->input->post(); 
      $id_negara = $post["id_negara"];
      $nama_negara = $post["nama_negara"];
      $negara_singkatan = $post["negara_singkatan"];

      $data_negara = $this->getAll();
      $cek = true;

      foreach($data_negara as $i){
        $id_temp = $i->id_negara;
        $nama_temp = $i->nama_negara;
        $singkatan_temp = $i->negara_singkatan;

        if($id_negara != $id_temp && ($nama_negara == $nama_temp || $negara_singkatan == $singkatan_temp)){
            $cek = false;
            break;
        }
      }


      if($cek == true){
        $query = $this->db->query("
          UPDATE master_negara
          SET nama_negara = ".$this->db->escape($nama_negara).",
              negara_singkatan = ".$this->db->escape($negara_singkatan)."
          WHERE id_negara = $id_negara
        ");
        return true;
      }
      else{
        return false;
      }

    }

    public function delete($id){
      //method ini menghapus data pada tabel berdasarkan id
      return $this->db->delete($this->_table,array("id_negara" => $id));
    }

    function jumlah_data_negara(){
      return $this->db->get('master_negara')->num_rows();
    }
      
    function get_data_negara($number,$offset){
      $this->db->order_by("id_negara desc");
      return $query = $this->db->get('master_negara',$number,$offset)->result();		
    }
}