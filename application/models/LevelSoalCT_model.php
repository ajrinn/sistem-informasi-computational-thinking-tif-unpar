<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LevelSoalCT_model extends CI_Model{

    private $_table = "master_level_soal";

    public function getAll(){
       return $this->db->get($this->_table)->result();
         //Artinya seperti select * from master_level_soal
    }

    public function getById($id){
      $query = $this->db->query("
                                  SELECT *
                                  from master_level_soal
                                  where master_level_soal.id_level_soal = $id;
      ");
      $res = $query->row();
      return $res;
    }

    public function validate_form(){
      $this->form_validation->set_rules('level_soal','Level Soal CT','required');
      $this->form_validation->set_rules('keterangan','Keterangan','required');
      
      $validation = $this->form_validation->run();
      if($validation == TRUE){
        return TRUE;
      }
      else{
        return FALSE;
      }

    }

    public function save(){

      $post = $this->input->post(); 
      $level_soal = $post["level_soal"];
      $keterangan = $post["keterangan"];

      $data_level = $this->getAll();
      $cek = true;

      $this->db->query('alter table master_level_soal AUTO_INCREMENT=0');

      foreach($data_level as $i){
        $level_temp = $i->level_soal;
        $keterangan_temp = $i->keterangan;

        if($level_soal == $level_temp || $keterangan == $keterangan_temp){
          $cek = false;
          break;
        }

      }

      if($cek == true){
        $query = $this->db->query("
                                  INSERT INTO master_level_soal(level_soal,keterangan)
                                  VALUES ('$level_soal','$keterangan');
        ");

        return true;
      }else{
        return false;
      }

  }

    public function update(){
      $post = $this->input->post(); 
      $id_level_soal = $post["id_level_soal"];
      $level_soal = $post["level_soal"];
      $keterangan = $post["keterangan"];

      $data_level = $this->getAll();
      $cek = true;

      foreach($data_level as $i){
        $id_temp = $i->id_level_soal;
        $level_temp = $i->level_soal;
        $keterangan_temp = $i->keterangan;

        if($id_level_soal != $id_temp && ($level_soal == $level_temp || $keterangan == $keterangan_temp)){
          $cek = false;
          break;
        }

      }

      if($cek == true){
        $query = $this->db->query("
          UPDATE master_level_soal
          SET level_soal = ".$this->db->escape($level_soal).",
              keterangan = ".$this->db->escape($keterangan)."
          WHERE id_level_soal = $id_level_soal
        ");
        
        return true;
      }
      else{
        return false;
      }

    }

    public function delete($id){
      //method ini menghapus data pada tabel berdasarkan id
      return $this->db->delete($this->_table,array("id_level_soal" => $id));
    }

    function jumlah_data_level(){
      return $this->db->get('master_level_soal')->num_rows();
      }
      
    function get_data_level($number,$offset){
        $this->db->order_by("id_level_soal desc");
        return $query = $this->db->get('master_level_soal',$number,$offset)->result();		
    }
}