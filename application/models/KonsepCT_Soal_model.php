<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KonsepCT_Soal_model extends CI_Model{

    private $_table = "konsep_soal";

    function insert_konsep_soal ($id_soal_ct,$id_konsep_ct){
        $data = array('id_soal_ct' => $id_soal_ct, 'id_konsep_ct' => $id_konsep_ct);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;
    }

    public function update_konsep_soal($id_soal_ct,$id_konsep_ct){
        $query = $this->db->query("UPDATE konsep_soal SET id_konsep_ct = '$id_konsep_ct' WHERE id_soal_ct = '$id_soal_ct'");
        return $query;
    }

    public function delete_konsep_soal($id_soal_ct){
        $query = $this->db->query("
                                    DELETE FROM konsep_soal 
                                    WHERE konsep_soal.id_soal_ct = '$id_soal_ct';
        ");
    }

    function getKonsepCT_by_idSoalCT($id_soal_ct){
        $query = $this->db->query("
                                SELECT master_konsep_ct.konsep_ct
                                FROM master_konsep_ct JOIN konsep_soal
                                ON master_konsep_ct.id_konsep_ct = konsep_soal.id_konsep_ct
                                WHERE konsep_soal.id_soal_ct = '$id_soal_ct';
        ");
        
        $res = $query->result();
        return $res;

    }

    function get_idKonsepCT_by_idSoalCT($id_soal_ct){
        $query = $this->db->query("
                                    SELECT konsep_soal.id_konsep_ct
                                    FROM konsep_soal
                                    WHERE konsep_soal.id_soal_ct = '$id_soal_ct';
        ");
        $res = $query->result();
        return $res;
    }

    





}