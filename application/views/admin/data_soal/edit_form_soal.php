<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>

	<style>
	            .tipe_gambar{
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }

						.alert-info{
                margin-left:0px;
                margin-right:10px;
            }

						.demoInputBox {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                
            }

						.keteranganGambar {
                padding: 3px 5px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }
	</style>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

			<!--breadcrumbs disini-->

			<!--Bagian session-->
				<?php if ($this->session->flashdata('gagal_kategori_level_soal')): ?>
				<div class="alert alert-danger" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_kategori_level_soal'); ?>
				</div>
				<?php elseif ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
			 	  <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_tipe_file_salah')): ?>
				<div class="alert alert-danger" role="alert">
				  <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_tipe_file_salah'); ?>
				</div>
        <?php elseif ($this->session->flashdata('gagal_file_terlalu_besar')): ?>
				<div class="alert alert-danger" role="alert">
			  	<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_file_terlalu_besar'); ?>
				</div>
				<?php endif; ?>



				<br>	
				<h5 class="font-weight-bold text-center">Sunting Data Soal CT dan Pembahasan Soal CT</h5>
				<br>
				
				<form action="<?php base_url('admin/soal_ct/edit') ?>"  id="form_soal" method="post" enctype="multipart/form-data">

				<!-- Card  -->
				<div class="card mb-3">	
					<div class="card-header">

						<a href="<?php echo site_url('admin/soal_ct/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>
					<div class="card-header">
            <h5 class="text-center">Soal CT</h5>
          </div>


					<div class="card-body">

					<?php foreach ($soal_ct as $key):?>
							
							<input type="hidden" name="id_soal_ct" value="<?php echo $key->id_soal_ct?>" />

                <div class="form-group">
								<label for="judul_soal">Judul Soal*</label>
								<input class="form-control 
								<?php echo form_error('judul_soal') ? 'is-invalid':'' ?>"
								 type="text" name="judul_soal" placeholder="Judul Soal" value="<?php echo htmlspecialchars($key->judul_soal)?>" />
								<div class="invalid-feedback">
									<?php echo form_error('judul_soal') ?>
								</div>
							</div>

							<div class="form-group">
							<?php echo form_error('tipe_soal') ? 'is-invalid':'' ?>
								<label for="tipe_soal">Tipe Soal*</label>
								<div>
                         <select name="tipe_soal" id="tipe_soal" class="demoInputBox" class="dropdown-menu">
														<?php
														$tipeSoal = $key->tipe_soal;
														if($tipeSoal === "Pilihan Ganda"){
																echo '
																	<option class="dropdown-item" value="Pilihan Ganda" selected>Pilihan Ganda</option>
																	<option class="dropdown-item" value="Jawaban Singkat">Jawaban Singkat</option> 
																';
														}
														else{
															echo '
															<option class="dropdown-item" value="Pilihan Ganda" selected>Pilihan Ganda</option>
															<option class="dropdown-item" value="Jawaban Singkat" selected>Jawaban Singkat</option> 
														';
														}
														
														?>												 
												 </select>
								</div>
								<div class="invalid-feedback">
									<?php echo form_error('tipe_soal') ?>
								</div>

								
							</div>

							

							<div class="form-group">
								<label for="deskripsi_soal">Deskripsi Soal*</label>
								<textarea class="form-control 
								<?php echo form_error('deskripsi_soal') ? 'is-invalid':'' ?>"
								 type="text" name="deskripsi_soal" id="deskripsi_soal" placeholder="Deskripsi Soal" rows="10" cols="80" />
								 <?php echo $key->deskripsi_soal?>
								 </textarea>
								 <script type="text/javascript">
                				   // Replace the <textarea id="editor1"> with a CKEditor
                				   // instance, using default configuration.

													 CKEDITOR.replace( 'deskripsi_soal',{
														enterMode: CKEDITOR.ENTER_BR
													 });

   								</script>
            					

								<div class="invalid-feedback">
									<?php echo form_error('deskripsi_soal') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="pertanyaan">Pertanyaan*</label>
								<textarea class="form-control 
								<?php echo form_error('pertanyaan') ? 'is-invalid':'' ?>"
								 type="text" name="pertanyaan" id="pertanyaan" placeholder="Pertanyaan" rows="10" cols="80"/>
								 <?php echo $key->pertanyaan?>
								 </textarea>
								 <script>
                				   // Replace the <textarea id="editor1"> with a CKEditor
                				   // instance, using default configuration.
                				   CKEDITOR.replace( 'pertanyaan',{
														enterMode: CKEDITOR.ENTER_BR
													 });
            					</script>
								<div class="invalid-feedback">
									<?php echo form_error('pertanyaan') ?>
								</div>
							</div>

							<div class="form-group">

							<label>Tahun Soal*</label>
							<div>
								<select name="tahun" id="tahun" class="demoInputBox">
									<?php
									
										$max = 2019;

										$tahun_selected = $key->tahun;
									    $tahun = 0;
										for($i = 2004 ; $i <= $max ; $i++ ) {
											$tahun = $i;

											if($tahun_selected == $tahun){
												echo '<option value='.$i.' selected>'.$i.'</option>';
											}
											else{
												echo '<option value='.$i.'>'.$i.'</option>';
											}
											   
										}
									?> 
								</select>
							</div>
							<br>

					<label for="negara_asal_soal">Negara Asal Soal*</label>							
                    <div>
                         <select name="negara_asal_soal" id="negara_asal_soal" class="demoInputBox">

														<?php
														   foreach ($negara_pembuat_soal as $i) {
																	$id_negara_selected = $key->id_negara_asal_soal;
																	$id_negara = $i->id_negara;
																 if($id_negara_selected == $id_negara){
																	echo '<option value='.$i->id_negara.' selected>'.$i->nama_negara.'</option>';
																 }
																 else{
																	echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
																 }
															 }
														?>

                         </select>
                     </div>
					 <br>

							<?php echo form_error('negara_pembuat_soal') ? 'is-invalid':'' ?>
					<label for="negara_pembuat_soal">Negara Pembuat Soal*</label>							
                    <div>
                         <select name="negara_pembuat_soal" id="negara_pembuat_soal" class="demoInputBox">

														<?php
														   foreach ($negara_pembuat_soal as $i) {
																	$id_negara_selected = $key->id_negara_pembuat_soal;
																	$id_negara = $i->id_negara;
																 if($id_negara_selected == $id_negara){
																	echo '<option value='.$i->id_negara.' selected>'.$i->nama_negara.'</option>';
																 }
																 else{
																	echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
																 }
															 }
														?>

                         </select>
                     </div>

										 <div class="invalid-feedback">
												<?php echo form_error('negara_pembuat_soal') ?>
										</div>
							</div>
							
							<div class="form-group">
								<label for="konsepCT[]">Konsep Computational Thinking</label><span id="konsepCT-error" class="datasoalpembahasan-error"></span>
                    
                  <?php foreach ($konsep_ct as $i) :?>
                     <div class="form-check">
  					   			 <input class="form-check-input" type="checkbox" name="konsepCT[]" value="<?php echo $i -> id_konsep_ct?>" id="konsep<?php echo $i -> id_konsep_ct?>"
												<?php 
													foreach ($id_konsep_ct as $j){
														$id_konsep = $i -> id_konsep_ct;
														$id_konsep_checked = $j -> id_konsep_ct;
														if($id_konsep == $id_konsep_checked){
															echo 'checked';
														}
														else{
															echo '';
														}
													}
												?>
										 >
  					    		 <label class="form-check-label" for="konsep<?php echo $i -> id_konsep_ct?>"> <?php echo $i -> konsep_ct?> </label>
										</div>
                  <?php endforeach;?>
							
							</div>

							<div class="form-group">
							 <label for="tagCT[]">Tags (Kata Kunci)</label><span id="tagCT-error" class="datasoalpembahasan-error"></span>           				
                    
                <div class="row">
                  <div class="col">

												<?php
														$batas1 = ceil($jumlah_tag/4); 
														$count = 1;		
														foreach ($tag_ct as $i): 
															if($count>=1 && $count<=$batas1) : 
																//echo $count;	
                        ?>
                        <div class="form-check">
  					       					 <input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>"
															<?php 
																	foreach ($id_tag as $j){
																			$id_tag_ct = $i -> id_tag;
																			$id_tag_checked = $j -> id_tag;
																			if($id_tag_ct == $id_tag_checked){
																					echo 'checked';
																				}
																			else{
																					echo '';
																			}
																	}
																?>
															>
  					        			 	 <label class="form-check-label" style = "font-size:14px" for="tag<?php  echo $i->id_tag?>"> <?php echo  $i -> tag?> </label>
					    					</div>

                      <?php   $count++;  endif; endforeach; 	?>

										
        
                  </div>
                  <div class="col">
                        
                      <?php 
														$batas1 = ceil($jumlah_tag/4); 
														$count = $batas1+1;		
														foreach (array_slice($tag_ct, $batas1)  as $i): 
															if($count>$batas1 && $count<=2*$batas1) : 
																//echo $count;	
                        ?>
                        <div class="form-check">
  					        				<input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>"
														<?php 
																	foreach ($id_tag as $j){
																			$id_tag_ct = $i -> id_tag;
																			$id_tag_checked = $j -> id_tag;
																			if($id_tag_ct == $id_tag_checked){
																					echo 'checked';
																				}
																			else{
																					echo '';
																			}
																	}
																?>
														>
  					        				<label class="form-check-label" style = "font-size:14px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    					</div>
												<?php   $count++;  endif; endforeach; 	?>

                  </div>
                  <div class="col">                         
                     
										 <?php 
														$batas1 = ceil($jumlah_tag/4); 
														$count = (2*$batas1)+1;		
														foreach (array_slice($tag_ct, (2*$batas1))  as $i): 
															if($count>2*$batas1 && $count<=3*$batas1) : 
																//echo $count;	
                        ?>
                        <div class="form-check">
  					        				<input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>"
														<?php 
																	foreach ($id_tag as $j){
																			$id_tag_ct = $i -> id_tag;
																			$id_tag_checked = $j -> id_tag;
																			if($id_tag_ct == $id_tag_checked){
																					echo 'checked';
																				}
																			else{
																					echo '';
																			}
																	}
																?>
														>
  					       				  <label class="form-check-label" style = "font-size:14px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					   					 </div>
										<?php   $count++;  endif; endforeach; 	?>
                        
                  </div>
                  <div class="col">
                                
									<?php 
														$batas1 = ceil($jumlah_tag/4); 
														$count = (3*$batas1)+1;		
														foreach (array_slice($tag_ct, (3*$batas1))  as $i): 
															if($count>3*$batas1 && $count<=4*$batas1) : 
																//echo $count;	
                        ?>
                        <div class="form-check">
  					        				<input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>"
														<?php 
														
																	foreach ($id_tag as $j){
																			$id_tag_ct = $i -> id_tag;
																			$id_tag_checked = $j -> id_tag;
																			if($id_tag_ct == $id_tag_checked){
																					echo 'checked';
																				}
																			else{
																					echo '';
																			}
																	}
																?>
														>
  					        				<label class="form-check-label" style = "font-size:14px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    					</div>
												<?php   $count++;  endif; endforeach; 	?>
                  </div>

                </div>							
							</div>

							<div class="form-group">
									<label>Kategori Umur dan Level Soal*</label>				
									
									<table class="table">
												<thead>
													<tr>
														<th scope="col">No.</th>
														<th scope="col">Nama Kategori Umur</th>
														<th scope="col">Level Soal</th>
													</tr>
												</thead>
												<tbody>
												

													<?php 
															if($kategori_level_soal != false){

												 			$num = 1;
													    	foreach($kategori_level_soal as $i){
																	/*
																	echo '<br>';
																	echo $i->id_soal_ct.' ';
																	echo $i->id_kategori_umur.' ';
																	echo $i->id_level_soal.' ';
																	echo '<br>';
																	*/
																
							
																	echo '<tr>';
																	echo'<td>'.$num.'</td>';
																	echo '<td>'.$i->nama_kategori.' ('.$i->umur_awal.' - '.$i->umur_akhir.' tahun / '.$i->negara_singkatan.')'.'</td>';
																	echo '<td>'.$i->level_soal.' ('.$i->keterangan.')'.'</td>';
																	echo '<td>';
																	echo ' <a class="text-danger delete0" href="#"> <i class="fas fa-trash-alt"></i> Delete </a>';
																	echo '</td>';
																	echo '</tr>';



																	echo '<a id="link" class="link_kategori" type="hidden" 
																	onclick="window.location.href=\'';
																	echo site_url('admin/soal_ct/delete_kategori_level_soal/'.$i->id_soal_ct.'/'.$i->id_kategori_umur.'/'.$i->id_level_soal);
																	echo '\'"></a> ';
																	$num++;									
																}
															}
														?>
												
												</tbody>
										  </table>
						
							</div>

							<div class="form-group">
									<div id="uploadFileContainer1"></div>
            			<div>
                    		<button id="ADDFILE1" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Kategori Umur dan Level Soal </button>    
              		</div>
							</div>
						
							<br>
						
						
		
							<?php 	
							$jumlah = count($image_ct);
							
							foreach ($image_ct as $i){
								
								$id_image = $i->id_image;
								//echo $id_image.'<br>';
							
               if($id_image != null){


									 echo "Tipe Gambar : Gambar ".$i->tipe_gambar;
									 echo '<a id="delete_gambar" class="text-danger delete1" style="float:right" href="#"> Delete Gambar <i class="fas fa-times text-danger"></i></a>';	
									 echo '<div style=" display:flex; justify-content:center; align-items:center; margin-top:15px">
									   <img style="width:auto;height:auto" src="data:image/jpeg;base64,'.$this->controller->display_gambar($id_image).'"/>
									</div>';
									 echo '<p class="text-center"> Keterangan Gambar : '.$i->keterangan_gambar.'</p>';
								   echo '<input type="hidden" name="id_image[]" value="'.$id_image.'" />
								   <label>Upload Gambar '.$i->tipe_gambar.' Baru*</label>
								  <div class="alert alert-info row">             									
										<div class="col">
											<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label><span id="jawaban-error"></span>	
											<br>										 
											<input type="file" style="height:66%" accept="image/*" name="multipleFiles[]" >
										</div>
									   <div class="col-lg-6" style="margin-left:20px">
									   <label>Tipe Gambar</label> <br>
											<select name="tipe_gambar[]" id="tipe_gambar" class="tipe_gambar" class="dropdown-menu" style="width:auto">
												<option class="dropdown-item" value="Soal (Deskripsi Soal)"> Soal (Deskripsi Soal)</option>  
												<option class="dropdown-item" value="Soal (Pertanyaan)"> Soal (Pertanyaan)</option>                                                                           
											</select> 
										 </div>
										 <div style="margin-top:10px;"> 
												<label style="margin-left:17px">Keterangan Gambar : </label> <br>
												<input type="text" name="keteranganGambar[]" style="margin-left:17px;width:500px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>
										 </div>
									</div> ';

									
								}

									echo '<a id="link2" class="link_gambar" type="hidden" onclick="window.location.href=\'';
									echo site_url('admin/soal_ct/delete_image/'.$id_image);
									echo'\'"></a>';
							

							}
								
							?>


							<br>
							
							<div class="form-group" >
									<div id="uploadFileContainer2" ></div>
									<div>
												<button id="ADDFILE2" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar Soal</button>    
									</div>
							</div>
					</div>
			
					<?php endforeach;?>
            
				<div class="card"></div>
							
						  <div class="card-header">
            		<h5 class="text-center">Pembahasan Soal CT</h5>
          		</div>
				
							
						<div class="card-body">
								<input type="hidden" name="id_pembahasan" value="<?php echo $pembahasan_soal_ct->id_pembahasan?>" />
								<input type="hidden" name="id_pembahasanSoal" value="<?php echo $pembahasan_soal_ct->id_soal_ct?>" />
							<div class="form-group">
								<label for="Jawaban">Jawaban*</label>
								<input class="form-control 
								<?php echo form_error('jawaban') ? 'is-invalid':'' ?>"
								 type="text" name="jawaban" id="jawaban" placeholder="Jawaban" value="<?php echo $pembahasan_soal_ct->jawaban?>" />
								<div class="invalid-feedback">
									<?php echo form_error('jawaban') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="penjelasan">Penjelasan*</label>
								<textarea class="form-control 
								<?php echo form_error('penjelasan') ? 'is-invalid':'' ?>"
								 type="text" name="penjelasan" id="penjelasan" placeholder="Penjelasan" />
								 <?php echo $pembahasan_soal_ct->penjelasan?>
								 </textarea>
								 <script>
                				   // Replace the <textarea id="editor1"> with a CKEditor
                				   // instance, using default configuration.
                				   	CKEDITOR.replace( 'penjelasan',{
									enterMode: CKEDITOR.ENTER_BR
									});
            					</script>
								<div class="invalid-feedback">
									<?php echo form_error('penjelasan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="penjelasan_ct">Hubungan dengan Computational Thinking*</label>
								<textarea class="form-control 
								<?php echo form_error('penjelasan') ? 'is-invalid':'' ?>"
								 type="text" name="penjelasan_ct" id="penjelasan_ct" placeholder="Hubungan dengan Computational Thinking (CT)" />
								 <?php echo $pembahasan_soal_ct->penjelasan_ct?>
								 </textarea>
								 <script>
                				   // Replace the <textarea id="editor1"> with a CKEditor
                				   // instance, using default configuration.
                				   CKEDITOR.replace( 'penjelasan_ct',{
									enterMode: CKEDITOR.ENTER_BR
									});
            					</script>
								<div class="invalid-feedback">
									<?php echo form_error('penjelasan_ct') ?>
								</div>
							</div>

						
							<?php 
							
							foreach ($image_pembahasan as $i){					
							 $id_image = $i->id_image;
              		if($id_image != null){
									 echo "Gambar ".$i->tipe_gambar;
								   echo '<a id="delete_gambar_pembahasan"  style="float:right" class="text-danger delete2" href="#"> Delete Gambar <i class="fas fa-times text-danger"></i></a>';							 
									 echo '<div style="display:flex; justify-content:center; align-items:center; margin-top:15px">
									       <img style="width:auto;height:auto" src="data:image/jpeg;base64,'.$this->controller->display_gambar($id_image).'"/>
											 	 </div>';
									echo '<p class="text-center"> Keterangan Gambar : '.$i->keterangan_gambar.'</p>';
									echo 
									 '<input type="hidden" name="id_image2[]" value="'.$id_image.'" />
								   <label>Upload Gambar '.$i->tipe_gambar.' Baru*</label>
								    <div class="alert alert-info row">             
										<div class="col">
														<label><strong>Pilih Gambar</strong>(Ukuran maksimum 2 MB)</label><span id="jawaban-error"></span>
														<br>											 
														<input type="file" style="height:66%" accept="image/*" name="multiplePembahasan[]" >
										 </div>
									   <div class="col" style="margin-left:20px">
													<label>Tipe Gambar</label> <br>
														<select name="tipe_gambarPembahasan[]" id="tipe_gambarPembahasan" class="demoInputBox"  class="dropdown-menu" style="width:auto">
														<option class="dropdown-item" value="Pembahasan (Jawaban)"> Pembahasan (Jawaban)</option> 
														<option class="dropdown-item" value="Pembahasan (Penjelasan Jawaban)"> Pembahasan (Penjelasan Jawaban)</option>                                                                                   
														</select> 
										 </div>
										 <div style="margin-top:10px"> 
												<label style="margin-left:17px;">Keterangan Gambar : </label> <br>
												<input type="text" name="keteranganGambarPembahasan[]" style="margin-left:17px;width:500px" id="keteranganGambarPembahasan" class="keteranganGambar" placeholder="Keterangan Gambar"/>
										 </div>
									</div> ';

								}

								echo '<a id="link3" class="link_pembahasan" type="hidden"
								onclick="window.location.href=\'';
								echo site_url('admin/soal_ct/delete_image_pembahasan/'.$id_image);
								echo '\'"></a> ';		
								$id_image=0;
							}

								
               ?>

							<br>

							<div class="form-group">
									<div id="uploadFileContainer3"></div>
            			<div>
                    		<button id="ADDFILE3" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar Pembahasan </button>    
              		</div>
							</div>
						  
							<br>
								
							<br>
				
							
							</div>
					 </div>

				
							
							<div class="text-center" style="margin:30px 0">
								<input class="btn btn-primary" type="submit" name="btn" value="Save" />
							</div>
							

						</form>
						

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php //$this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>

$(document).ready(function(){

	$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});

	$(document).on('click','button#ADDFILE1', function(event){
				event.preventDefault();
				addFileInput1();
		});                 

		function addFileInput1() {
			var html = '';
			var html = '';
            html+='<div class="alert alert-info row" >';            
            html+='<div class="col">';
            html+='<label>Kategori Umur</label> <br>';
            html+='<select name="kategori_umur[]" id="kategori_umur" class="demoInputBox" class="dropdown-menu">';
            html+='<?php foreach ($kategori_umur as $i): ?>'; 
            html+='<option class="dropdown-item" value="<?php echo $i->id_kategori_umur?>"> <?php echo $i->nama_kategori?> (<?php echo $i->umur_awal?> - <?php echo $i->umur_akhir?> tahun / <?php echo $i->negara_singkatan?>) </option>';
            html+='<?php endforeach; ?>';                                           
            html+='</option>';                                                                             
            html+='</select>'; 
            html+='</div>'; 
            html+='<div class="col" style="margin-left:20px">';
            html+='<label>Level Soal</label> <br>';
            html+='<select name="level_soal[]" id="level_soal" class="demoInputBox" class="dropdown-menu" style="width:auto">';
            html+='<?php foreach ($level_soal as $i): ?>'; 
            html+='<option class="dropdown-item" value="<?php echo $i->id_level_soal?>"> <?php echo $i->level_soal?> (<?php echo $i->keterangan?>) </option>';
            html+='<?php endforeach; ?>';                                                                             
            html+='</select>'; 
            html+='</div>';
            html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'; 
            html+='</div>';
			$("div#uploadFileContainer1").append(html);
		}

	$(document).on('click','button#ADDFILE2', function(event){
				event.preventDefault();
				addFileInput2();
		});                 

		function addFileInput2() {
			var html = '';
            html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label><br>';
            html+='<input type="file" accept="image/*" name="multipleFiles2[]">';
            html+='</div>';   
            html+='<div class="col" style="margin-left:20px">';
            html+='<label>Tipe Gambar</label> <br>';
            html+='<select name="tipe_gambar2[]" id="tipe_gambar" class="tipe_gambar" class="dropdown-menu" style="width:auto">';
            html+='<option class="dropdown-item" value="Soal (Deskripsi Soal)"> Soal (Deskripsi Soal)</option>';  
            html+='<option class="dropdown-item" value="Soal (Pertanyaan)"> Soal (Pertanyaan)</option>';                                                                            
            html+='</select>'; 
            html+='</div>';
						html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
						html+='<div style="margin-top:10px;">'; 
						html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
						html+='<input type="text" name="keteranganGambar2[]" style="margin-left:17px;width:500px" id="keteranganGambar2" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
						html+='</div>';
            html+='</div>';
			$("div#uploadFileContainer2").append(html);
		}


		$(document).on('click','button#ADDFILE3', function(event){
				event.preventDefault();
				addFileInput3();
		});                 

		function addFileInput3() {
			var html = '';
						html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label><br>';
            html+='<input type="file" accept="image/*" name="multiplePembahasan2[]">';
            html+='</div>';   
            html+='<div class="col" style="margin-left:20px">';
            html+='<label>Tipe Gambar</label> <br>';
            html+='<select name="tipe_gambarPembahasan2[]" id="tipe_gambarPembahasan2" class="tipe_gambar" class="dropdown-menu" style="width:auto">';
            html+='<option class="dropdown-item" value="Pembahasan (Jawaban)"> Pembahasan (Jawaban)</option>'; 
    				html+='<option class="dropdown-item" value="Pembahasan (Penjelasan Jawaban)"> Pembahasan (Penjelasan Jawaban)</option>';                                                                             
            html+='</select>'; 
            html+='</div>';
            html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
						html+='<div style="margin-top:10px;">'; 
						html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
						html+='<input type="text" name="keteranganGambarPembahasan2[]" style="margin-left:17px;width:500px" id="keteranganGambarPembahasan2" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
						html+='</div>';
            html+='</div>';
			$("div#uploadFileContainer3").append(html);
		}
		




	
	//delete kategori umur dan level soal
	 var a=0;
		$('.delete0').each(function() {
			a++;
			$(this).attr("id", "ku"+a);
		});

		var b = 0;
			$('.link_kategori').each(function() {
				b++;
				$(this).attr("name", "ku"+b);
			});

		$(".delete0").click(function() {
			var id_link0 = this.id;

				$('.link_kategori').each(function() {
					var val0 = this.name;

					if(val0 == id_link0){
							$('a[name="' + val0 + '"]').click();
							$("#form_soal").submit();	
						}

					//alert(val0);
				});

				//alert(id_link0);
		});
 

 //delete gambar soal ct
		var i=0;
		$('.delete1').each(function() {
			i++;
			$(this).attr("id", i);
		});

		var j = 0;
			$('.link_gambar').each(function() {
				j++;
				$(this).attr("name", j);
			});

		$(".delete1").click(function() {
			var id_link = this.id;

				$('.link_gambar').each(function() {
					var val = this.name;

					if(val == id_link){
							$('a[name="' + val + '"]').click();
							$("#form_soal").submit();	
						}

					//alert(val);
				});

				//alert(id_link);
		});
	
 //delete gambar pembahasan
 var n = 0;
		$('.delete2').each(function() {
			n++;
			$(this).attr("id", "link"+n);
		});

var m = 0;
			$('.link_pembahasan').each(function() {
				m++;
				$(this).attr("name", "link"+m);
			});

		$(".delete2").click(function() {
			var id_link2 = this.id;

				$('.link_pembahasan').each(function() {
					var val2 = this.name;

					if(val2 == id_link2){
							$('a[name="' + val2 + '"]').click();
							$("#form_soal").submit();	
						}

					//alert(val);
				});

			//	alert(id_link2);
		});
		
});





		

</script>


</html>