<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>

	<style>
			 .demoInputBox {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: auto;
            }

	</style>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!--breadcrumbs disini-->

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
				    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_kategori_umur')): ?>
				<div class="alert alert-danger" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_kategori_umur'); ?>
				</div>
				<?php endif; ?>
				
				<br>
				<h5 class="font-weight-bold text-center">Tambah Kategori Umur</h5>
				<br>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/kategori_umur_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/kategori_umur_ct/add') ?>" method="post">
							<div class="form-group">
								<label for="nama_kategori">Nama Kategori Umur*</label>
								<input class="form-control <?php echo form_error('nama_kategori') ? 'is-invalid':'' ?>"
								 type="text" name="nama_kategori" placeholder="Nama Kategori Umur" />
								<div class="invalid-feedback">
									<?php echo form_error('nama_kategori') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_negara">Negara*</label>							
								<div>
									<select name="id_negara" id="id_negara" class="demoInputBox">
										<?php
												foreach ($negara as $i) {
													echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
												}
										?>
									</select>
								</div>
							</div>

							<div class="form-group" style="width:auto;" >
								<label for="umurAwal">Umur Awal : </label>
								<div>
									<select id="umurAwal" name="umurAwal" class="demoInputBox">
										<option value="Pilih Umur Awal" disabled>Pilih Umur Awal : </option>
										<?php
											for($i = 6 ; $i < 18 ; $i++){										
												echo '<option value="'.$i.'">'.$i.' tahun </option>';                           
											}									
										?>
									</select> 
								</div> 
         				    </div>

							 <div class="form-group" style="width:auto;" >
								<label for="umur_akhir">Umur Akhir : </label>
								<div>
									<select id="umurAkhir" name="umurAkhir" class="demoInputBox">  
										<option value="Pilih Umur Akhir" disabled>Pilih Umur Akhir :  </option> 
										<?php
											for($i = 7 ; $i <=18 ; $i++){										
												echo '<option value="'.$i.'">'.$i.' tahun </option>';                           
											}									
										?>
									</select>							
								</div>
							</div>
							 

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php //$this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>										
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>
	$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});

		$("#umur_awal").change(function(){

			for(i=6; i<=18; i++) {
				$("#umur_akhir option[value='" + i +"']").remove();
			}
	
			var val = parseInt($("#umur_awal").val());
			
			for(i=val+1; i<=18; i++) {
				$("#umur_akhir").append("<option value='" + i + "'>"+i+" tahun </option>");
			}
	
			if(val == 18){
				$("#umur_akhir").append("<option value='" + val + "'>"+val+" tahun </option>");
			}

		});
     
	 });

	
</script>

</html>