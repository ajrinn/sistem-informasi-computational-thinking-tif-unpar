<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">


	<?php $this->load->view("admin/_partials/navbar.php") ?>

	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!--Breadcrumb Simpan disini-->
				<br>
				<h5 class="text-center text-bold font-weight-bold">Daftar Liputan CT</h5>
				<br>
				<!-- DataTables -->
				<div class="card mb-3">

					<div class="card-header">
						<a href="<?php echo site_url('admin/liputan_ct/add')?>"><i class="fas fa-plus"></i> Tambah Liputan CT</a>
					</div>
					
					<div class="card-body">

					<?php if (!empty($data_liputan)): ?>

						<div class="table-responsive">
							<table class="table table-bordered table-hover" id="table_liputan" width="100%" cellspacing="0">
								<thead>
									<tr>
                                        <th>No.</th>
										<th>Judul Liputan</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
									$count = 1;									
                                     foreach ($data_liputan as $i): ?>
									 
									<tr>
                                        <td>
											<?php 
											      if(($page > $count)){
													 $count = $page+1;													 
												  }
												  echo $count;
												  
											?>
										</td>
										<td>
											<?php echo $i->judul_liputan?>
										</td>
										<td width="250">
                                           <a href="<?php echo base_url();?>index.php/admin/liputan_ct/tampilkanDataLiputan/<?php echo $i->id_liputan?>"
											class="btn text-primary"><i class="fas fa-info-circle"></i> Lihat Liputan CT</a> <br>

											<a href="<?php echo site_url('admin/liputan_ct/edit/'.$i->id_liputan) ?>"
								            class="btn btn-small text-warning"><i class="fas fa-edit"></i> Sunting Liputan CT</a>

											<a onclick="deleteConfirm('<?php echo site_url('admin/liputan_ct/delete/'.$i->id_liputan) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php $count++;  endforeach; ?>

								</tbody>
							</table>
						</div>

						<div class="row">
							<div class="col">
							<?php echo $pagination;?>
							</div>
						</div>

						<?php endif; ?>


					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php //$this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
		$('#btn-delete').attr('href',url);
		$('#deleteModal').modal();
		}

		$(document).ready(function(){
			
			
			$('#table_liputan').DataTable({
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 2 ],
					orderData: [ 2, 0 ]
				} ]
			});

			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
	</script>

</body>

</html>