<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
  <style> 
    #loadMore {
    padding: 10px;
    text-align: center;
    background-color: #33739E;
    color: #fff;
    border-width: 0 1px 1px 0;
    border-style: solid;
    border-color: #fff;
    box-shadow: 0 1px 1px #ccc;
    transition: all 600ms ease-in-out;
    -webkit-transition: all 600ms ease-in-out;
    -moz-transition: all 600ms ease-in-out;
    -o-transition: all 600ms ease-in-out;
}
#loadMore:hover {
    background-color: #fff;
    color: #33739E;
}

button.message{
  margin: auto;
  display:block;
  left:50%;
}

#konsepCTDiv {
  width: 100%;
  text-align: center;
  margin-top: 10px;
}

#tipeSoalDiv {
  width: 100%;
  text-align: center;
  margin-top: 10px;
}

#jenjangPendidikanDiv {
  width: 100%;
  text-align: center;
  margin-top: 10px;
}

.keterangan{
  margin-left:70px;
  margin-top:20px;
  margin-right:70px; 
  padding:15px;
}
  
  </style>
</head>

<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">

  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    
    <a class="navbar-brand " href="<?php echo site_url('user/home')?>">  Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>



<div class = "card-header"> 
       <h4 class = "text-center">Kategori Pencarian Soal Computational Thinking</h4>
</div>  

<div class="keterangan border">
<p><b>Keterangan :</b><br>
    - Untuk menghindari keambiguan, maka kategori pencarian jenjang pendidikan dan umur tidak bisa dipilih bersamaan, jika kategori pencarian jenjang pendidikan
    dipilih, maka <br>
    &ensp;kategori pencarian umur akan di-<i>disable</i>, begitu pula sebaliknya. 
 </p>
</div>




<form action="<?php echo site_url('user/soal_ct_user/') ?>" method="post">
  
    <div class="row" style = "padding-bottom:50px;padding-left:50px;padding-right:50px;padding-top:30px">  

            <div class="form-group col-md-auto center-block border">
                        <fieldset>
                            <span class="text-center">
                              <legend>Konsep CT</legend>
                            </span>
                            <div style="margin-top:-12px"></div>

                            <div class="checkbox checkbox-primary" id="konsepCTDiv">
                              <input type="checkbox" id="konsepAll" name="konsepAll" value="">
                              <label for="konsepAll">Select All</label>
                            </div>

                            <?php foreach ($konsep_ct as $i) :?>
                                <div class="checkbox checkbox-primary">
                                      <input class="form-check-input konsepCT" type="checkbox" name="konsepCT[]" 
                                      id="konsep<?php echo $i -> id_konsep_ct?>" value="<?php echo $i -> id_konsep_ct?>"
                                          <?php 
                                             
                                              foreach($selected_konsep as $j){
                                               
                                                $id_konsep = $i -> id_konsep_ct;
                                                $id_konsep_checked = $j;
                                                if($id_konsep == $id_konsep_checked){
                                                  echo 'checked';  
                                                }
                                                else{
                                                  echo '';
                                                }
                                              }
                                          ?>                            
                                         >
                                        <label  class="form-check-label" for="konsep<?php echo $i -> id_konsep_ct?>">
                                            <?php echo $i -> konsep_ct?>
                                        </label>
                                </div>
                            <?php endforeach;?>

                            
                           
                        </fieldset>  
               </div>
        
                <div class="form-group col-md-auto center-block border">
                    <fieldset>
                            <span class="text-center">
                              <legend>Berdasarkan Tipe Soal</legend>
                            </span>
                            <div style="margin-top:-12px"></div>

                                <div class="checkbox checkbox-primary" id="tipeSoalDiv">
                                  <input type="checkbox" id="tipeAll" name="tipeAll" value="">
                                  <label for="tipeAll">Select All</label>
                                </div>

                                <div class="checkbox checkbox-primary" style="margin-left:10px">
                                      <input name="tipeSoal[]" class="form-check-input tipeSoal" id="tipeSoal1" type="checkbox" value="Pilihan Ganda"
                                        <?php 
                                                foreach($selected_tipeSoal as $j){
                                                  $tipeSoal = "Pilihan Ganda";
                                                  $tipeSoal_checked = $j;
                                                  if($tipeSoal == $tipeSoal_checked){
                                                    echo 'checked';
                                                  }
                                                  else{
                                                    echo '';
                                                  }
                                                }
                                            ?>       
                                      >
                                        <label for="tipeSoal1">
                                            Pilihan Ganda
                                        </label>
                                </div>

                                <div class="checkbox checkbox-primary" style="margin-left:10px">
                                      <input name="tipeSoal[]" class="form-check-input tipeSoal" id="tipeSoal2" type="checkbox" value="Jawaban Singkat"
                                      <?php 
                                                foreach($selected_tipeSoal as $j){
                                                  $tipeSoal = "Jawaban Singkat";
                                                  $tipeSoal_checked = $j;
                                                  if($tipeSoal == $tipeSoal_checked){
                                                    echo 'checked';
                                                  }
                                                  else{
                                                    echo '';
                                                  }
                                                }
                                        ?>                   
                                      >
                                        <label for="tipeSoal2">
                                            Jawaban Singkat
                                        </label>
                                </div>

                          </fieldset>          
                </div>
          
                  <div class="form-group col-md-auto center-block border">
                          <fieldset>
                              <span class="text-center">
                                <legend>Jenjang Pendidikan</legend>
                              </span>
                              <div style="margin-top:-12px"></div>
                            
                            <div class="checkbox checkbox-primary" id="jenjangPendidikanDiv">
                               <input type="checkbox" id="jenjangAll" name="jenjangAll" value="">
                               <label for="jenjangAll">Select All</label>
                            </div>

                            <?php 
                              foreach ($jenjang_pendidikan as $i) :?>
                                  <div class="checkbox checkbox-primary">
                                        <input name="jenjangPendidikan[]" id="jenjangPendidikan<?php echo $i -> id_kategori_umur?>"
                                         type="checkbox" value="<?php echo $i -> id_kategori_umur?>"
                                              <?php 
                                                      foreach($selected_jenjangPendidikan as $j){
                                                        $jenjangPendidikan = $i->id_kategori_umur;
                                                        $jenjangPendidikan_checked = $j;
                                                                                                                 
                                                          if($jenjangPendidikan == $jenjangPendidikan_checked){
                                                            echo 'checked';
                                                          }
                                                          else{
                                                            echo '';
                                                          }                                        
                                                      }
                                              ?> 
                                        >
                                          <label for="jenjangPendidikan<?php echo $i -> id_kategori_umur?>">
                                            <?php echo $i -> nama_kategori?> (<?php echo $i ->umur_awal?> - <?php echo $i ->umur_akhir?> tahun)
                                          </label>
                                  </div>
                              <?php endforeach;?>
                                  
                 </fieldset>
          </div>



        <div class="col-md-auto center-block border" align="center">
        <fieldset>
              <span class="text-center">
              <legend>Berdasarkan Umur</legend>
              </span>
         
        <div class="col-md-auto">
          <div class="form-group" style="width:auto;" >
                <label for="umurAwal">Umur Awal : </label>
                <select class="form-control" id="umurAwal" name="umurAwal">
                       <option value="Pilih Umur Awal" disabled>Pilih Umur Awal : </option>
                       <!--<option value="" hidden></option>-->
                       <?php
                        for($i = 6 ; $i < 18 ; $i++){
                          if($i == $selected_umurAwal)
                            $selected = "selected";
                          else 
                            $selected = "";
                           
                            echo '<option value="'.$i.'"'.$selected.'>'.$i.' tahun </option>';                           
                        }
                       
                       ?>
                </select>   
           </div>
          </div>

          <div class="col-md-auto">
          <div class="form-group" style="width:auto;" >
                <label for="umurAkhir">Umur Akhir : </label>
                <select class="form-control" id="umurAkhir" name="umurAkhir">  
                      <option value="Pilih Umur Akhir" disabled>Pilih Umur Akhir :  </option> 
                      <?php
                                for($i = 7 ; $i <= 18 ; $i++){
                                     if($i == $selected_umurAkhir){
                                            $selected = "selected";
                                        }
                                    else {
                                              $selected = ""; 
                                            }
                                      echo '<option value="'.$i.'"'.$selected.'>'.$i.' tahun </option>';  
                                  }
                      ?>

                </select>
           </div>
          </div>

       </fieldset> 
      </div>

    </div>

      <div class="text-center" style="margin-bottom:30px">
                
                <button type="submit" name="btn" class="btn btn-primary" value="Button Cari Soal">
                    Cari Soal  <i class="fas fa-search"></i>
                </button>
      </div>

  </form>




   
    <div class = "card-header"> 
       <h4 class = "text-center">Soal Computational Thinking</h4>
    </div>
    <!--Bagian session-->
    <?php if ($this->session->flashdata('soal_tidak_ditemukan')): ?>
        <div class="alert alert-danger text-center" role="alert">
          <?php echo $this->session->flashdata('soal_tidak_ditemukan'); ?>
        </div>
    <?php endif; ?>

    <div class="post_wrap row" style="padding:50px">
       

      <?php if( !empty($soal_relevan) ) : ?>
       <?php foreach ($soal_relevan as $i): ?>
             <div class="pst col-lg-4 col-sm-6 mb-4" style="width: 24rem;">

             <div class="card h-100 text-center mb-3" id="card1">
               
                <?php
                  $id_soal_ct = $i->id_soal_ct;
                  $id_image = $this->controller->get_first_imageSoal($id_soal_ct);
                  $id_image_pembahasan = $this->controller->get_first_imagePembahasan($id_soal_ct);
                 
                  if($id_image != false){
                    echo '
                          <div style="width:auto;height:22rem; display:flex; justify-content:center; align-items:center; padding:20px">
                              <img class="img-fluid" 
                                   style="width:auto;height:18rem"
                                   src="data:image/jpeg;base64,'.$this->controller->display_gambar($id_image).'"/>
                           </div>
                          ';
                  }
                  else if($id_image_pembahasan != false){
                    echo '
                    <div style="width:auto;height:22rem; display:flex; justify-content:center; align-items:center; padding:20px">
                        <img class="img-fluid" style="width:auto;height:18rem" src="data:image/jpeg;base64,'.$this->controller->display_gambar($id_image_pembahasan).'"/>
                     </div>
                    ';
                  }
                  else{
                    echo '
                    <div style="width:auto;height:22rem; display:flex; justify-content:center; align-items:center; padding:20px">
                    <img class="img-fluid" style="width:auto;height:18rem" src="';
                    echo base_url();
                    echo '/assets/images/dekomposisi.jpg" alt="Card image cap">';
                    echo '</div>';
                  }
                
                ?>

                <div class="card-body" style="padding:20px">

                
                <h4>
                <a href="<?php echo base_url();?>index.php/user/soal_ct_user/page_soal_ct/<?php echo $i->id_soal_ct?>"><?php echo $i->judul_soal?></a>
                </h4>
                
                  
                  <?php

                      //untuk menampilkan konsep CT
                       echo ' <p class="card-text text-left" style="margin-top:25px">';
                       $res = '';
                        foreach($data_konsep_ct as $j){
                          $id_soal_ct = $i->id_soal_ct;
                          $id_soal = $j->id_soal_ct;
                          if($id_soal_ct == $id_soal){
                              $konsep_ct = $j->konsep_ct;
                              $res = $res.', '.$konsep_ct;
                          }
                        }
                        $res=trim($res,', ');
                        echo '<strong> Konsep CT : </strong>'.' '.$res;
                      
                      echo '</p>';

                      //untuk menampilkan kategori umur dan level soal
                      echo ' <p class="card-text text-left">';
                       $res = '';
                       echo '<strong> Kategori Umur : </Strong><br>';
                        foreach($data_kategori_umur as $j){
                          $id_soal_ct = $i->id_soal_ct;
                          $id_soal = $j->id_soal_ct;
                          if($id_soal_ct == $id_soal){
                              $kategori_umur = $j->nama_kategori;
                              $umur_awal = $j->umur_awal;
                              $umur_akhir = $j->umur_akhir;
                              $level_soal = $j->level_soal;
                              $negara_singkatan = $j->negara_singkatan;
                              if($level_soal != 'Umum'){
                              $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / Level '.$level_soal.' / '.$negara_singkatan.')';
                              echo $res.'<br>';
                              }
                              else{
                              $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / '.$level_soal.' / '.$negara_singkatan.')';
                              echo $res.'<br>';
                              } 

                          }
                        }
                      echo '</p>';

                    //untuk menampilkan asal soal / asal bebras
                      echo '<p class="card-text text-left">';
                      $res = '';
                      echo '<strong> Asal Soal : </Strong>';
                      foreach($soal_ct as $j){
                        $id_soal_ct = $i->id_soal_ct;
                        $id_soal = $j->id_soal_ct;
                        $tahun = $j->tahun;
                        if($id_soal_ct == $id_soal){
                            $nama_negara = $j->nama_negara;
                            $res ="Bebras ".$nama_negara." (".$tahun.")";
                        }
                      }
                      echo $res.'</p>';
                      
                    //untuk menampilkan tag soal
                    echo ' <p class="card-text text-left">';
                       $res = '';
                        foreach($data_tag_soal as $j){
                          $id_soal_ct = $i->id_soal_ct;
                          $id_soal = $j->id_soal_ct;
                          if($id_soal_ct == $id_soal){
                              $tag = $j->tag;
                              $res = $res.', '.$tag;
                          }
                        }
                        $res=trim($res,', ');
                        echo '<strong> Kata Kunci : </strong>'.' '.$res;
                      echo '</p>';
                      
                   ?>

                      <p class="card-text text-left">
                        <strong>Tipe Soal : </strong> <?php echo $i->tipe_soal?>
                      </p>
                    
                      

              </div>

               <div style="margin-bottom:25px">
                  <a href="<?php echo base_url();?>index.php/user/soal_ct_user/laporan_pdf/<?php echo $i->id_soal_ct?>" class="btn btn-success" role="button" aria-pressed="true"><i class="fas fa-arrow-down"></i> Download</a>
               </div>
               
            </div>
        </div>

      
              
        <?php  endforeach; ?>
        <?php endif; ?>

        <button type="button" class="message btn btn-primary" style="margin-top:20px" id="button1"></button>
      

        
    </div>
        
   

    </div>

  

    <?php $this->load->view("user/_partials/footer.php") ?> 



<script>

$(document).ready(function(){


 $("#umurAwal").change(function(){
        
      for(i=7; i<=18; i++) {
        $("#umurAkhir option[value='" + i +"']").remove();
      }

      var val = parseInt($("#umurAwal").val());
      
      for(i=val+1; i<=18; i++) {
            $("#umurAkhir").append("<option value='" + i + "'>"+i+" tahun </option>");
      }

      if(val == 18){
        $("#umurAkhir").append("<option value='" + val + "'>"+val+" tahun </option>");
      }

        $("#jenjangPendidikan6").prop("disabled", true);
        $("#jenjangPendidikan7").prop("disabled", true);
        $("#jenjangPendidikan8").prop("disabled", true);
        $("#jenjangAll").prop("disabled",true);
       
  });


    $("#umurAkhir").change(function(){
          $("#jenjangPendidikan6").prop("disabled", true);
          $("#jenjangPendidikan7").prop("disabled", true);
          $("#jenjangPendidikan8").prop("disabled", true);
          $("#jenjangAll").prop("disabled",true);
    });

    
    if( !$('#jenjangAll').is(':checked') || !$('#jenjangPendidikan6').is(':checked') && !$('#jenjangPendidikan7').is(':checked') && !$('#jenjangPendidikan8').is(':checked')){
        // checked
          $("#umurAwal").prop("disabled", false);
          $("#umurAkhir").prop("disabled", false);
        }
        else{
        // unchecked
              $("#umurAwal").prop("disabled", true);
              $("#umurAkhir").prop("disabled", true);
        }

    $("input[name='jenjangPendidikan[]'], #jenjangAll").click(function() {
      if ($(this).is(":checked")) {
          $("#umurAwal").prop("disabled", true);
          $("#umurAkhir").prop("disabled", true);
      } 
      else{
        if(!$('#jenjangAll').is(':checked') || !$('#jenjangPendidikan6').is(':checked') && !$('#jenjangPendidikan7').is(':checked') && !$('#jenjangPendidikan8').is(':checked')){
        // checked
          $("#umurAwal").prop("disabled", false);
          $("#umurAkhir").prop("disabled", false);
        }
        else{
        // unchecked
              $("#umurAwal").prop("disabled", true);
              $("#umurAkhir").prop("disabled", true);
        }
      }
   });

   $ShowHideMore = $('.post_wrap');
   $ShowHideMore.each(function() {
    var $times = $(this).children('.pst');
    if ($times.length > 9) {
        $ShowHideMore.children(':nth-of-type(n+10)').addClass('moreShown').hide();
        $(this).find('button.message').addClass('more-times').html('+ Show more');
    }
    else{
      $('#button1').hide();
    }
  });

  $(document).on('click', '.post_wrap > button', function() {
    var that = $(this);
    var thisParent = that.closest('.post_wrap');
    if (that.hasClass('more-times')) {
      thisParent.find('.moreShown').show();
      that.toggleClass('more-times', 'less-times').html('- Show less');
    } else {
      thisParent.find('.moreShown').hide();
      that.toggleClass('more-times', 'less-times').html('+ Show more');
    }  
  });
  

    $( '#topheader .navbar-nav a' ).on( 'click', function () {
      $( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
      $( this ).parent( 'li' ).addClass( 'active' );
    });
    

//Checkbox Konsep CT
  $('#konsepAll').click(function(){
      if(this.checked){   
        $('input[name*=konsepCT]').each(function(){
          this.checked = true;
        });
      }else{
        $('input[name*=konsepCT]').each(function(){
          this.checked = false;
        });
      }
  });

  $('input[name*=konsepCT]').click(function(){
    var temp = 0;
    $('input[name*=konsepCT]').each(function(){
       if(this.checked == true){
        temp++;
       }
    });

    var jumlahKonsep = $("input[name='konsepCT[]']").length;
    
    if(temp != jumlahKonsep){
      $('#konsepAll').prop('checked', false);
    }
    else{
      $('#konsepAll').prop('checked', true);
    }
});

    var jumlahKonsepTotal = $("input[name='konsepCT[]']").length;
    var jumlahKonsepChecked = $("input:checkbox:checked.konsepCT").length;

    if(jumlahKonsepTotal === jumlahKonsepChecked){
      $('#konsepAll').prop('checked', true);
    }
    else{
      $('#konsepAll').prop('checked', false);
    }

 
 
//Checkbox Tipe Soal
    $('#tipeAll').click(function(){
      if(this.checked){
        $('input[name*=tipeSoal]').each(function(){
          this.checked = true;
        });
      }else{
        $('input[name*=tipeSoal]').each(function(){
          this.checked = false;
        });
      }
    });

    

    $('input[name*=tipeSoal]').click(function(){
      var temp2 = 0;
      var jumlahTipe = 0;
    
      $('input[name*=tipeSoal]').each(function(){
        if(this.checked == true){
          temp2++;
        }
        jumlahTipe++;
      });
      
      if(temp2 != jumlahTipe){
        $('#tipeAll').prop('checked',false);
      }
      else{
        $('#tipeAll').prop('checked',true);
      }

    });

    var jumlahTipeTotal = $("input[name*='tipeSoal']").length;
    var jumlahTipeChecked = $("input[name*='tipeSoal']:checked").length;

    if(jumlahTipeTotal === jumlahTipeChecked){
      $('#tipeAll').prop('checked', true);
    }
    else{
      $('#tipeAll').prop('checked', false);
    }


//Checkbox Jenjang Pendidikan
    $('#jenjangAll').click(function(){
      if(this.checked){
        $('input[name*=jenjangPendidikan]').each(function(){
          this.checked = true;
        });
      }
      else{
        $('input[name*=jenjangPendidikan]').each(function(){
          this.checked = false;
        });
      }
    });

    $('input[name*=jenjangPendidikan]').click(function(){
      var temp3 = 0;
      var jumlahJenjang = 0;
      $('input[name*=jenjangPendidikan]').each(function(){
        if(this.checked == true){ 
          temp3++;
        }
        jumlahJenjang++;
      });

      if(temp3 != jumlahJenjang){
        $('#jenjangAll').prop('checked',false);
      }
      else{
        $('#jenjangAll').prop('checked',true);
      }
    });

    var jumlahJenjangTotal = $("input[name*='jenjangPendidikan']").length;
    var jumlahJenjangChecked = $("input[name*='jenjangPendidikan']:checked").length;

    if(jumlahJenjangTotal === jumlahJenjangChecked){
      $('#jenjangAll').prop('checked', true);
    }
    else{
      $('#jenjangAll').prop('checked', false);
    }

});
</script>