
<ul class="sidebar navbar-nav"> 
   
   <!-- <li class="nav-item <?php echo $this->uri->segment(2) == 'admin' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin/overview') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Overview</span>
        </a>
    </li>-->
    
    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'admin' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-user-alt"></i>
            <span>Kelola Konten Soal CT dan Pembahasan</span>
        </a>
        <ul class="dropdown-menu" aria-labelledby="pagesDropdown">
            <li><a class="dropdown-item"  href="<?php echo site_url('admin/soal_ct/add') ?>">Tambah Soal CT dan Pembahasan</a></li>
            <li><a class="dropdown-item"  href="<?php echo site_url('admin/soal_ct') ?>">Daftar Soal CT</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('admin/konsep_ct/') ?>">Daftar Konsep CT</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('admin/kategori_umur_ct/') ?>">Daftar Kategori Umur</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('admin/level_soal_ct/') ?>">Daftar Level Soal</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('admin/tag_ct/') ?>">Daftar Tag (Kata Kunci)</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('admin/negara/') ?>">Daftar Negara</a></li>


            <!--
            <li class ="dropdown-submenu dropdown-item">
                <a  href="#" style="text-decoration:none;color:black" class="test droptown-toggle"
                 data-toggle="dropdown">Properti Soal CT<span>
               </span></a>
                    <ul class="dropdown-menu" style="margin-left:0px">
                            <li><a class="dropdown-item" href="<?php echo site_url('admin/konsep_ct/') ?>">Daftar Konsep CT</a></li>
                            <li><a class="dropdown-item" href="<?php echo site_url('admin/kategori_umur_ct/') ?>">Daftar Kategori Umur</a></li>
                            <li><a class="dropdown-item" href="<?php echo site_url('admin/level_soal_ct/') ?>">Daftar Level Soal</a></li>
                            <li><a class="dropdown-item" href="<?php echo site_url('admin/tag_ct/') ?>">Daftar Tag</a></li>
                            <li><a class="dropdown-item" href="<?php echo site_url('admin/negara/') ?>">Daftar Negara</a></li>
                    </ul>
                </a>
            </li>
            -->

        </ul>

    </li>




    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'admin' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-user-alt"></i>
            <span>Kelola Konten Liputan CT oleh TIF UNPAR</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/liputan_ct/add') ?>">Tambah Liputan CT</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/liputan_ct/') ?>">Daftar Liputan CT</a>
        </div>

    </li>

    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'admin' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-user-alt"></i>
            <span>Kelola Data Admin</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/admin/add') ?>">Tambah Admin</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/admin') ?>">Daftar Admin</a>
        </div>
    </li>


</ul>

