<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SoalCT_model extends CI_Model{

    private $_table = "master_soal_ct";
    private $id_soal_ct;
    private $judul_soal;
    private $tipe_soal;
    private $deskripsi_soal;
    private $pertanyaan;
    private $negara_pembuat_soal;
    private $tahun_soal;
    private $negara_asal_soal;

    private $id_pembahasan;
    private $jawaban;
    private $penjelasan;

    public function __construct(){
        parent::__construct();
        $this->load->model("UploadGambar_model");
        $this->load->model("PembahasanSoalCT_model");
        $this->load->model('ImageSoalCT_model');
        $this->load->model("TagCT_soal_model");
        $this->load->model("KonsepCT_Soal_model");
        $this->load->model("KategoriLevelSoalCT_model");
        $this->load->model("Negara_model");
        $this->load->library("form_validation"); //meload library form validation
    }

    public function getAll(){

        //return $this->db->get($this->_table)->result();
         //Artinya seperti select * from master_soal_ct

         $query = $this->db->query("
                                    SELECT master_soal_ct.id_soal_ct,
                                            master_soal_ct.judul_soal,
                                            master_soal_ct.tipe_soal,
                                            master_soal_ct.deskripsi_soal,
                                            master_soal_ct.pertanyaan,
                                            master_soal_ct.tahun,
                                            master_negara.id_negara,
                                            master_negara.nama_negara
                                    FROM 
                                        master_soal_ct join master_negara
                                        on  master_soal_ct.id_negara_asal_soal = master_negara.id_negara
                                    ORDER BY 
                                        master_soal_ct.id_soal_ct desc;
        ");

        return $query->result();
    }


    public function getByID($id){
        //$query = $this->db->get_where($this->_table,["id_soal_ct"=>$id]);
        $query = $this->db->query("
                                    SELECT master_soal_ct.id_soal_ct,
                                            master_soal_ct.judul_soal,
                                            master_soal_ct.tipe_soal,
                                            master_soal_ct.deskripsi_soal,
                                            master_soal_ct.pertanyaan,
                                            master_soal_ct.tahun,
                                            master_soal_ct.id_negara_asal_soal,
                                            master_soal_ct.id_negara_pembuat_soal
                                    FROM 
                                        master_soal_ct
                                    WHERE master_soal_ct.id_soal_ct = $id;
        ");
        return $query->result();

        //Artinya seperti select * from admin where id_soal_ct = $id
        //method yang mengembalikan sebuah objek dari row tabel
    }

    function per_id($id) {
        $this->db->where('id_soal_ct',$id);
        $query=$this->db->get('master_soal_ct');
        return $query->result();
    }




    public function save(){
        //method ini menyimpan data ke tabel master_soal_ct
        $temp = $this->db->insert_id();
        $post = $this->input->post();                                //mengambil data dari form
        $this->id_soal_ct =  $temp;               
        $this->id_pembahasan = $temp;                                //data id_pembahasan dari form
        $this->judul_soal = $post["judul_soal"];                     //data judul_soal dari form
        $this->tipe_soal = $post["tipe_soal"];                       //data tipe_soal dari form
        $this->deskripsi_soal = $post["deskripsi_soal"];             //data deksripsi_soal dari form
        $this->pertanyaan = $post["pertanyaan"];                     //data pertanyaan dari form
        $this->tahun_soal = $post["tahun"];
        $this->negara_asal_soal = $post["negara_asal_soal"];   
        $this->negara_pembuat_soal = $post["negara_pembuat_soal"];   //data negara_pembuat_soal dari form      
        $this->db->insert($this->_table,$this);                      //melakukan insert data ke tabel master_soal_ct
        $this->db->query('alter table master_soal_ct AUTO_INCREMENT 0');

    }

    public function validate_form(){

        $maxsize = 2097152;
        $max_img_soal = 0;
        $max_img_pembahasan = 0;
        $max_img_soal2 = 0;
        $max_img_pembahasan2 = 0; 
        
        //arr untuk menampung size
        $arr_edit_soal = [];
        $arr_edit_pembahasan = [];
        $arr_tambah_soal = [];
        $arr_tambah_pembahasan = [];

        //arr untuk menampung type
        $arr_editSoal_type = [];
        $arr_editPembahasan_type = [];
        $arr_tambahSoal_type = [];
        $arr_tambahPembahasan_type = [];

        $cek_editSoal_type = "true";
        $cek_editPembahasan_type = "true";
        $cek_tambahSoal_type = "true";
        $cek_tambahPembahasan_type = "true";

        $res = false;


        if($this->input->post()){
            $this->form_validation->set_rules('judul_soal','Judul Soal','required');
            $this->form_validation->set_rules('tipe_soal','Tipe Soal','required');
            $this->form_validation->set_rules('deskripsi_soal','Deskripsi Soal', 'required');
            $this->form_validation->set_rules('pertanyaan','Pertanyaan', 'required');
            $this->form_validation->set_rules('negara_pembuat_soal','Negara Pembuat Soal', 'required');

            //print_r($this->input->post());
    
            //validasi edit gambar soal
            if(!empty($_FILES['multipleFiles']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multipleFiles']['size']); $i++){
                    $arr_edit_soal[$i] = $_FILES['multipleFiles']['size'][$i];
                    if($arr_edit_soal[$i] > $max_img_soal){
                        $max_img_soal = $arr_edit_soal[$i]; 
                    }

                    $arr_editSoal_type[$i] = $_FILES['multipleFiles']['type'][$i];
                    if($arr_editSoal_type[$i] == "image/jpeg" || $arr_editSoal_type[$i] == "image/gif" || $arr_editSoal_type[$i] == "image/png" || empty( $arr_editSoal_type[$i])){
                        $cek_editSoal_type = "true";
                    }
                    else{
                        $cek_editSoal_type = "false";
                        break;
                    }
                }

                //print_r($_FILES['multipleFiles']['size']);
            }

            //validasi edit gambar pembahasan
            if(!empty($_FILES['multiplePembahasan']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multiplePembahasan']['size']); $i++){
                    $arr_edit_pembahasan[$i] = $_FILES['multiplePembahasan']['size'][$i];
                    if($arr_edit_pembahasan[$i] > $max_img_pembahasan){
                        $max_img_pembahasan = $arr_edit_pembahasan[$i]; 
                    }

                    $arr_editPembahasan_type[$i] = $_FILES['multiplePembahasan']['type'][$i];
                    if($arr_editPembahasan_type[$i] == "image/jpeg" || $arr_editPembahasan_type[$i] == "image/gif" || $arr_editPembahasan_type[$i] == "image/png" || empty( $arr_editPembahasan_type[$i])){
                        $cek_editPembahasan_type = "true";
                    }
                    else{
                        $cek_editPembahasan_type = "false";
                        break;
                    }
                
                }

               // print_r($_FILES['multiplePembahasan']['size']);
            }

            //validasi tambah gambar soal
            if(!empty($_FILES['multipleFiles2']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multipleFiles2']['size']); $i++){
                    $arr_tambah_soal[$i] = $_FILES['multipleFiles2']['size'][$i];
                    if($arr_tambah_soal[$i] > $max_img_soal2){
                        $max_img_soal2 = $arr_tambah_soal[$i]; 
                    }

                    $arr_tambahSoal_type[$i] = $_FILES['multipleFiles2']['type'][$i];
                    if($arr_tambahSoal_type[$i] == "image/jpeg" || $arr_tambahSoal_type[$i] == "image/gif" || $arr_tambahSoal_type[$i] == "image/png" || empty( $arr_tambahSoal_type[$i])){
                        $cek_tambahSoal_type = "true";
                    }
                    else{
                        $cek_tambahSoal_type = "false";
                        break;
                    }
                }

                //print_r($_FILES['multipleFiles2']['size']);
            }


            //validasi tambah gambar pembahasan
            if(!empty($_FILES['multiplePembahasan2']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multiplePembahasan2']['size']); $i++){
                    $arr_tambah_pembahasan[$i] = $_FILES['multiplePembahasan2']['size'][$i];
                    if($arr_tambah_pembahasan[$i] > $max_img_pembahasan2){
                        $max_img_pembahasan2 = $arr_tambah_pembahasan[$i]; 
                    }

                    $arr_tambahPembahasan_type[$i] = $_FILES['multiplePembahasan2']['type'][$i];
                    if($arr_tambahPembahasan_type[$i] == "image/jpeg" || $arr_tambahPembahasan_type[$i] == "image/gif" || $arr_tambahPembahasan_type[$i] == "image/png" || empty( $arr_tambahPembahasan_type[$i])){
                        $cek_tambahPembahasan_type = "true";
                    }
                    else{
                        $cek_tambahPembahasan_type = "false";
                        break;
                    }
                }

                //print_r($_FILES['multiplePembahasan2']['size']);
            }
            

            /*
            echo '<br> max_img_soal = '.$max_img_soal.'<br>';
            echo 'max_img_pembahasan = '.$max_img_pembahasan.'<br>';
            echo 'max_img_soal2 = '.$max_img_soal2.'<br>';
            echo 'max_img_pembahasan2 = '.$max_img_pembahasan2.'<br>';
            echo $maxsize;
            */

            /*
            if($this->session->has_userdata('success')){
                $this->session->unset_userdata('success');
            }
            */

           // $test = $this->session->has_userdata('success');

            if($this->form_validation->run() == TRUE){

                if($cek_editSoal_type == 'true' && $cek_editPembahasan_type == 'true' && $cek_tambahSoal_type == 'true' && $cek_tambahPembahasan_type == 'true'){

                        if ($max_img_soal <= $maxsize && $max_img_pembahasan <= $maxsize && $max_img_soal2 <= $maxsize && $max_img_pembahasan2 <= $maxsize) {
                            //echo "test";
                            if($this->session->has_userdata('success') == FALSE){
                                $this->session->sess_destroy();
                                $this->session->set_flashdata('success', 'Sukses : Berhasil disimpan');
                            }
                            return TRUE; 
                        } 
                        else {
                            if($this->session->has_userdata('gagal_file_terlalu_besar') == FALSE){
                                $this->session->sess_destroy();
                                $this->session->set_flashdata('gagal_file_terlalu_besar', 'Gagal : Ukuran gambar terlalu besar, mohon pastikan ukuran gambar untuk soal CT dan pembahasan dibawah 2 MB!');                          
                            }
                        }
                    }
                else{
                    if($this->session->has_userdata('gagal_tipe_file_salah') == FALSE){
                        $this->session->sess_destroy();
                        $this->session-> set_flashdata('gagal_tipe_file_salah', 'Gagal : Tipe file tidak sesuai, mohon pastikan tipe gambar untuk soal CT dan pembahasan adalah jpeg/png/gif !');  
                    }
                }
            }
            else{
                $this->session->sess_destroy(); 
            }
        

        }

  

    }

    public function update(){
        //$this->session->sess_destroy();
        //method ini mengupdate data pada tabel soal_ct
        //$temp = $this->db->insert_id();

       

        //$post = $this->input->post();                                //mengambil data dari form
        if($this->input->post()){
            $post = $this->input->post();                                //mengambil data dari form
            //print_r($post);
            $this->id_soal_ct =  $post["id_soal_ct"];              
            $this->id_pembahasanSoal = $post["id_pembahasan"];               //data id_pembahasan dari form
            $this->judul_soal = $post["judul_soal"];                     //data judul_soal dari form
            $this->tipe_soal = $post["tipe_soal"];                       //data tipe_soal dari form
            $this->deskripsi_soal = $post["deskripsi_soal"];             //data deksripsi_soal dari form
            $this->pertanyaan = $post["pertanyaan"];                     //data pertanyaan dari form
            $this->tahun_soal = $post["tahun"];
            $this->negara_pembuat_soal = $post["negara_pembuat_soal"];   //data negara_pembuat_soal dari form
            $this->negara_asal_soal = $post["negara_asal_soal"];

            $this->id_pembahasan = $post['id_pembahasan'];           //data id_pembahasan dari form
            $this->jawaban = $post['jawaban'];                       //data jawaban dari form
            $this->penjelasan = $post['penjelasan'];                 //data penjelasan dari form
            $this->penjelasan_ct = $post['penjelasan_ct'];                 //data penjelasan dari form
        }

        
        $kategori_level_soal = $this->KategoriLevelSoalCT_model->get_kategoriLevelSoal($this->id_soal_ct);

     
        //update item konsep CT


        if (!empty($this->input->post('konsepCT'))) {
            $delete_konsepCT = $this->KonsepCT_Soal_model->delete_konsep_soal($this->id_soal_ct);

            foreach ($this->input->post('konsepCT') as $key => $val) {
                $dataKonsepCT[] = array( 'konsepCT' => $_POST['konsepCT'][$key]);
            }

            foreach ($dataKonsepCT as $item) {    
                $idSoal = $this->id_soal_ct;
                $dataKonsepCT = $this->KonsepCT_Soal_model->insert_konsep_soal($idSoal,$item['konsepCT']);
                }
        
        }

        

        
        //update item tag CT
        if(!empty($this->input->post('tagCT'))){
            $delete_tagCT =  $this->TagCT_Soal_model->delete_tag_soal($this->id_soal_ct);

            foreach($this->input->post('tagCT') as $key1 => $val1){
                $dataTagCT[] = array('tagCT' => $_POST['tagCT'][$key1]);
            }

            foreach($dataTagCT as $item){
                $idSoal = $this->id_soal_ct;
                $tagCT = $this->TagCT_Soal_model->insert_tag_soal($idSoal,$item['tagCT']);
                }
        
        }




        //Tambah Kategori Umur dan Level Soal
        if(!empty($this->input->post('kategori_umur'))){
            foreach($this->input->post('kategori_umur') as $key2 => $val2){
                $dataKategoriLevel[] = array('kategori_umur' => $_POST['kategori_umur'][$key2],'level_soal' => $_POST['level_soal'][$key2]);
            }   

            foreach($dataKategoriLevel as $item){
                $idSoalCT =  $this->id_soal_ct;
                $check = false;
            
            if($kategori_level_soal != false){
            foreach($kategori_level_soal as $i){
                $id_soal = $i->id_soal_ct;
                $id_kategori_umur = $i->id_kategori_umur;
                $id_level_soal = $i->id_level_soal;
            
                        if($idSoalCT ==  $id_soal){                   
                            if( $id_kategori_umur ==  $item['kategori_umur']){
                                if(  $id_level_soal ==  $item['level_soal']){                                               
                                    $check = false;
                                    break;
                                }                       
                                else{
                                    $check = true;
                                }
                            }
                            else{
                                $check = true;
                            }
                        }
                        else{
                            $check = true;
                        }
            }
            }
            else{
                $check = true; 
            }

                if($check == true){
                    $insert_KategoriLevel = $this->KategoriLevelSoalCT_model->insert_kategori_level_soal($idSoalCT,$item['kategori_umur'],$item['level_soal']); 
                }
                else{
                    $this->session->set_flashdata('gagal_kategori_level_soal', 'Terjadi kesalahan : Kategori umur dan level soal tidak boleh sama!');
                }
            }

        }
        


        //Tambah gambar soal pada edit soal form

            if(!empty($_FILES['multipleFiles2']['tmp_name'])){
                $idSoal =  $this->id_soal_ct;
            for($i = 0 ; $i < count($_FILES['multipleFiles2']['tmp_name']); $i++){
                $data1[] = array('multipleFiles2' => $_FILES['multipleFiles2']['tmp_name'][$i],
                                'tipe_gambar2' => $_POST['tipe_gambar2'][$i],
                            'keteranganGambar2' => $_POST['keteranganGambar2'][$i]);
            }

            foreach($data1 as $item){
                $idSoal =  $this->id_soal_ct;
                $image = $item['multipleFiles2'];
                $tipe_gambar = $item['tipe_gambar2'];
                $keteranganGambar = $item['keteranganGambar2'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                }
                else{
                    $image = null;
                }
                
                $setAutoIncrement = $this->UploadGambar_model->resetAI();

                if($image != null){
                    $dataGambar = $this->UploadGambar_model->insert_gambar($image,$tipe_gambar,$keteranganGambar);
                    $idGambar = $this->UploadGambar_model->getLastImageID();
                    $dataSoalGambar = $this->ImageSoalCT_model->insert_image_soal($idSoal,$idGambar);
                }


            }
        }


        //Tambah gambar pembahasan baru pada edit soal form

        if(!empty($_FILES['multiplePembahasan2']['tmp_name'])){
            
            for($i = 0 ; $i < count($_FILES['multiplePembahasan2']['tmp_name']); $i++){
                $data4[] = array('multiplePembahasan2' => $_FILES['multiplePembahasan2']['tmp_name'][$i],
                                'tipe_gambarPembahasan2' => $_POST['tipe_gambarPembahasan2'][$i],
                                'keteranganGambarPembahasan2' => $_POST['keteranganGambarPembahasan2'][$i]);
            }

            foreach($data4 as $item){
            // $idSoal = $this->SoalCT_model->getLastID();
                $idPembahasan =  $this->id_pembahasan;
                $image = $item['multiplePembahasan2'];
                $tipe_gambar = $item['tipe_gambarPembahasan2'];
                $keteranganGambar = $item['keteranganGambarPembahasan2'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                }
                else{
                    $image = null;
                }
                
                $setAutoIncrement = $this->UploadGambar_model->resetAI();

                if($image != null){
                    $dataGambar = $this->UploadGambar_model->insert_gambar($image,$tipe_gambar,$keteranganGambar);
                    $idGambar = $this->UploadGambar_model->getLastImageID();
                    $dataSoalGambar = $this->ImagePembahasanCT_model->insert_image_pembahasan($idPembahasan,$idGambar);
                }
            }

                  
        
        }



        //Edit gambar soal pada edit soal form

            if(!empty($_FILES['multipleFiles']['tmp_name'])){
            for($i = 0 ; $i < count($_FILES['multipleFiles']['tmp_name']); $i++){
                $dataGambarSoal[] = array('id_image'=>$post['id_image'][$i],
                                        'multipleFiles' => $_FILES['multipleFiles']['tmp_name'][$i],
                                        'tipe_gambar' => $post['tipe_gambar'][$i],
                                        'keteranganGambar' => $post['keteranganGambar'][$i]);
            }
            
            foreach($dataGambarSoal as $item){
                $idSoal = $this->id_soal_ct;
                $id_image = $item['id_image'];
                $image = $item['multipleFiles'];
                $tipe_gambar = $item['tipe_gambar'];
                $keteranganGambar = $item['keteranganGambar'];
                
                if($image != null){
                    $image = file_get_contents( $image );
                    $image = base64_encode($image);
                    $updateSoalGambar = $this->UploadGambar_model->update_image_soal($id_image,$image,$tipe_gambar,$keteranganGambar); 
                
                }
                else{
                    $image = null;
                }

                
            }    
         }
                
        
        //Edit soal pembahasan pada edit soal form

            if(!empty($_FILES['multiplePembahasan']['tmp_name'])){
                for($i = 0 ; $i < count($_FILES['multiplePembahasan']['tmp_name']); $i++){
                $data3[] = array('id_image2'=>$post['id_image2'][$i],
                                    'multiplePembahasan' => $_FILES['multiplePembahasan']['tmp_name'][$i],
                                    'tipe_gambarPembahasan' => $post['tipe_gambarPembahasan'][$i],
                                    'keteranganGambarPembahasan' => $post['keteranganGambarPembahasan'][$i]);
                }

                foreach($data3 as $item){
                    $idSoal = $this->SoalCT_model->getLastID();
                    $id_image = $item['id_image2'];
                    $image = $item['multiplePembahasan'];
                    $tipe_gambar = $item['tipe_gambarPembahasan'];
                    $keteranganGambarPembahasan = $item['keteranganGambarPembahasan'];
                            
                    if($image != null){
                            $image = file_get_contents( $image );
                            $image = base64_encode($image);
                            $updateSoalGambar = $this->UploadGambar_model->update_image_soal($id_image,$image,$tipe_gambar,$keteranganGambarPembahasan); 
                    }
                    else{
                            $image = null;
                    }               
                }
            }
                        
            $this->update_soal_ct($this->id_soal_ct,
                                  $this->judul_soal,
                                  $this->tipe_soal,
                                  $this->deskripsi_soal,
                                  $this->pertanyaan,
                                  $this->tahun_soal,
                                  $this->negara_asal_soal,
                                  $this->negara_pembuat_soal);
            
            $this->PembahasanSoalCT_model->update_pembahasan_soal_ct($this->input->post('id_pembahasan'),
                                                                     $this->jawaban,$this->penjelasan,
                                                                     $this->penjelasan_ct);
    }
        
    

    public function delete($id){
        //method ini menghapus data dengan id_soal_ct = $id pada  tabel master_soal_ct
        $this->db->query('alter table master_image_ct AUTO_INCREMENT=0');
       
        $this->db->delete($this->_table,array("id_soal_ct" => $id));
        $this->db->query('alter table master_soal_ct AUTO_INCREMENT=0');
    }

    public function getLastID(){
        $query = $this->db->query("SELECT id_soal_ct FROM master_soal_ct ORDER BY id_soal_ct DESC LIMIT 1");
        $id = $query->row()->id_soal_ct;
        return $id;
    }


    function insert_soal_ct($judul_soal,$tipe_soal,$deskripsi_soal,$pertanyaan,$tahun,$id_negara_asal_soal,$id_negara_pembuat_soal){        
        $sql = "
                INSERT INTO master_soal_ct (judul_soal, 
                                            tipe_soal, 
                                            pertanyaan, 
                                            deskripsi_soal,
                                            tahun, 
                                            id_negara_asal_soal,
                                            id_negara_pembuat_soal)
                VALUES (". $this->db->escape($judul_soal).",'$tipe_soal','$pertanyaan','$deskripsi_soal','$tahun','$id_negara_asal_soal','$id_negara_pembuat_soal');
               ";

        $query = $this->db->query("$sql");
        
        if($query !== NULL){
            return TRUE;
        }
        return FALSE;

    }

    public function update_soal_ct($id_soal_ct,$judul_soal,$tipe_soal,$deskripsi_soal,$pertanyaan,$tahun,$id_negara_asal_soal,$id_negara_pembuat_soal){

        $query = $this->db->query("
                                    UPDATE master_soal_ct
                                    SET master_soal_ct.judul_soal = ".$this->db->escape($judul_soal).",
                                        master_soal_ct.tipe_soal = '$tipe_soal',
                                        master_soal_ct.deskripsi_soal = '$deskripsi_soal',
                                        master_soal_ct.pertanyaan = '$pertanyaan',
                                        master_soal_ct.tahun = '$tahun',
                                        master_soal_ct.id_negara_asal_soal = '$id_negara_asal_soal',
                                        master_soal_ct.id_negara_pembuat_soal = '$id_negara_pembuat_soal'
                                   WHERE master_soal_ct.id_soal_ct = '$id_soal_ct';
        ");

    }

    
    function get_kategori_umur(){
        $query = $this->db->query("
                                    SELECT  master_soal_ct.id_soal_ct,
                                            master_soal_ct.judul_soal,
                                            master_kategori_umur.id_kategori_umur,
                                            master_kategori_umur.nama_kategori,
                                            master_kategori_umur.umur_awal,
                                            master_kategori_umur.umur_akhir,
                                            master_negara.nama_negara,
                                            master_negara.negara_singkatan,
                                            master_level_soal.level_soal,
                                            master_level_soal.keterangan
                                FROM 
                                    master_soal_ct JOIN kategori_level_soal
                                    on master_soal_ct.id_soal_ct = kategori_level_soal.id_soal_ct JOIN master_kategori_umur 
                                    on kategori_level_soal.id_kategori_umur = master_kategori_umur.id_kategori_umur JOIN master_negara
                                    on master_kategori_umur.id_negara = master_negara.id_negara JOIN master_level_soal
                                    on kategori_level_soal.id_level_soal = master_level_soal.id_level_soal
                                group BY 
                                        master_soal_ct.id_soal_ct,
                                         master_kategori_umur.id_kategori_umur,
                                         master_level_soal.level_soal
                                ORDER BY 
                                        master_soal_ct.id_soal_ct;
        ");
        return $query->result();
    }

    function get_konsep_ct(){
        $query = $this->db->query("
                                    SELECT master_soal_ct.id_soal_ct,
                                           master_konsep_ct.konsep_ct
                                    FROM 
                                         master_soal_ct JOIN konsep_soal
                                         on master_soal_ct.id_soal_ct = konsep_soal.id_soal_ct JOIN master_konsep_ct 
                                         on konsep_soal.id_konsep_ct = master_konsep_ct.id_konsep_ct		 
                                    ORDER by 
                                         master_soal_ct.id_soal_ct,master_konsep_ct.id_konsep_ct;
        ");
        return $query->result();
    }
    
    function get_tag_soal(){
        $query = $this->db->query("
                                    SELECT master_soal_ct.id_soal_ct,
                                           master_tags.tag
                                    FROM 
                                        master_soal_ct JOIN tags_soal
                                        on master_soal_ct.id_soal_ct = tags_soal.id_soal_ct JOIN master_tags 
                                        on tags_soal.id_tag = master_tags.id_tag
                                    ORDER by 
                                        master_soal_ct.id_soal_ct;
                                    ");
        $res = $query->result();
        return $res;
    }



    function jumlah_data_soal(){
		return $this->db->get('master_soal_ct')->num_rows();
    }

    function get_data_soal($number,$offset){
        $query = $this->db->query("
                                    SELECT master_soal_ct.id_soal_ct,
                                            master_soal_ct.judul_soal,
                                            master_soal_ct.tipe_soal,
                                            master_soal_ct.deskripsi_soal,
                                            master_soal_ct.pertanyaan,
                                            master_soal_ct.tahun,
                                            master_negara.id_negara,
                                            master_negara.nama_negara
                                    FROM 
                                        master_soal_ct join master_negara
                                        on  master_soal_ct.id_negara_asal_soal = master_negara.id_negara
                                    ORDER BY 
                                        master_soal_ct.id_soal_ct desc
                                    LIMIT $number 
                                    OFFSET $offset;
        ");

		return $query->result();		
    }
    
    function get_negara_soal($id){
        $query = $this->db->query("
                                    SELECT master_negara.nama_negara
                                    FROM master_negara join master_soal_ct
                                    ON master_negara.id_negara = master_soal_ct.id_negara_pembuat_soal
                                    WHERE master_soal_ct.id_soal_ct = $id
        ");

        $res = $query->row();
        return $res->nama_negara;
    } 

    function get_judul_soal($id){
        $query = $this->db->query("
                                    SELECT 
                                            master_soal_ct.judul_soal
                                    FROM 
                                        master_soal_ct
                                    WHERE master_soal_ct.id_soal_ct = $id;
        ");

        $res = $query->row();
        return $res->judul_soal;

    }


}