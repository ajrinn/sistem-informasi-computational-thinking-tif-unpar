<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PencarianSoalCT_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->model("KonsepCT_Soal_model");
    $this->load->model('KategoriLevelSoalCT_model');
    $this->load->model("SoalCT_model");
    $this->load->model("TagCT_soal_model");
  }

  function get_selected_konsep(){
        $dataKonsepCT = [];
        $dataRes = [];
        
        //mengambil data konsep CT dari form
        if(!empty($this->input->post("konsepCT"))){
          $konsepCT = $this->input->post("konsepCT");
          foreach($konsepCT as $key => $val){
            $dataKonsepCT[] = array("konsepCT" => $_POST['konsepCT'][$key]); 
          }
        }

        $i=0;
        foreach($dataKonsepCT as $item){
          $dataRes[$i] = $item["konsepCT"];
          $i++;
        }
    
      return $dataRes;
  }

  function get_selected_tipeSoal(){
      $dataTipeSoal = [];
      $dataRes = [];

      //mengambil data tipe soal dari form
      if(!empty($this->input->post("tipeSoal"))){
          $tipeSoal = $this->input->post("tipeSoal");
        
          foreach($tipeSoal as $key => $val){

              $dataTipeSoal[] = array("tipeSoal" => $_POST['tipeSoal'][$key]); 

            }
      }

      $i = 0;
      foreach($dataTipeSoal as $item){
        $dataRes[$i] = $item["tipeSoal"];
        $i++;
      }

      return $dataRes;
  }

  function get_selected_jenjangPendidikan(){
    $dataJenjangPendidikan = [];
    $dataRes = [];

    //mengambil data jenjang pendidikan dari form
    if(!empty($this->input->post("jenjangPendidikan"))){

      $jenjangPendidikan = $this->input->post("jenjangPendidikan");

      foreach($jenjangPendidikan as $key => $val){

        $dataJenjangPendidikan[] = array("jenjangPendidikan" => $_POST['jenjangPendidikan'][$key]); 

      }
    }

    $i = 0;
    foreach($dataJenjangPendidikan as $item){
      $dataRes[$i] = $item["jenjangPendidikan"];
      $i++;
    }

    return $dataRes;
  }

  function get_selected_umurAwal(){
    $dataUmurAwal = $this->input->post("umurAwal");
    return $dataUmurAwal;
  }

  function get_selected_umurAkhir(){
    $dataUmurAkhir = $this->input->post("umurAkhir");
    return $dataUmurAkhir;
  }





  function search_soal_ct(){

    //menghapus data pada tabel konsep_ct_temp
    $this->delete_konsep_temp();

    //variabel untuk menyimpan data yang dikirim dari form
      $dataKonsepCT = [];
      $dataTipeSoal = [];
      $dataJenjangPendidikan = [];
      $dataRes0 = [];
      $dataRes1 = [];
      $dataRes2 = [];
      $dataUmurAwal;
      $dataUmurAkhir;

    //menyimpan data konsep CT yang dikirim dari form
      if(!empty($this->input->post("konsepCT"))){

          $konsepCT = $this->input->post("konsepCT");

          foreach($konsepCT as $key => $val){
            $dataKonsepCT[] = array("konsepCT" => $_POST['konsepCT'][$key]); 
          }

          $i=0;
          foreach($dataKonsepCT as $item){
            $dataRes0[$i] = $item["konsepCT"];
            $i++;
          }
      }
    
     //meyimpan data tipe soal yang dikirim dari form
      if(!empty($this->input->post("tipeSoal"))){

        $tipeSoal = $this->input->post("tipeSoal");

        foreach($tipeSoal as $key => $val){

          $dataTipeSoal[] = array("tipeSoal" => $_POST['tipeSoal'][$key]); 

        }

        $i=0;
        foreach($dataTipeSoal as $item){
          $dataRes1[$i] = $item["tipeSoal"];
          $i++;
        }
      }



    //menyimpan data jenjang pendidikan yang dikirim dari form
    if(!empty($this->input->post("jenjangPendidikan"))){

      $jenjangPendidikan = $this->input->post("jenjangPendidikan");

      foreach($jenjangPendidikan as $key => $val){

        $dataJenjangPendidikan[] = array("jenjangPendidikan" => $_POST['jenjangPendidikan'][$key]); 

      }

      $i=0;
      foreach($dataJenjangPendidikan as $item){
        $dataRes2[$i] = $item["jenjangPendidikan"];
        $i++;
      }
    }

    //menyimpan data umur awal dan umur akhir yang dikirim dari form
    
    $dataUmurAwal = $this->input->post('umurAwal');
    $dataUmurAkhir = $this->input->post('umurAkhir');
    

 //insert dataKonsepCT ke table konsep_ct_temp
     foreach ($dataKonsepCT as $item){
      $id = $item['konsepCT'];
      $this->insert_konsep_temp($id);
    }
    

 //mendapatkan soal yang paling relevan berdasarkan konsep CT yang dipilih user   

    $data_relevan = $this->get_soalRelevan($dataRes0,$dataRes1,$dataRes2,$dataUmurAwal,$dataUmurAkhir);
    
    return $data_relevan;

  }


  function get_soalRelevan ($konsepCT,$tipeSoal,$jenjangPendidikan,$umurAwal,$umurAkhir){


    //untuk cek kategori seleksi
       $konsepCT_cek = count($konsepCT);
       $tipeSoal_cek = count($tipeSoal);
       $jenjangPendidikan_cek = count($jenjangPendidikan);
       $umurAwal_cek;
       if($umurAwal != null){
        $umurAwal_cek = "true";
       }else{
        $umurAwal_cek = "false";
       }


       $tipeSoal = implode("','", $tipeSoal);
       $jenjangPendidikan = implode(", ", $jenjangPendidikan);
     
      
      //menampung query yang akan dijalankan
      $query;
      

      //menampung query select apabila user memilih kategori pencarian konsep CT
      $query_konsep_true = "SELECT DISTINCT konsep_soal.id_soal_ct, 
                                  master_soal_ct.judul_soal, 
                                  master_soal_ct.tipe_soal,
                                  count(konsep_soal.id_konsep_ct) AS jumlah
                                  FROM konsep_soal JOIN konsep_ct_temp
                                    on konsep_soal.id_konsep_ct = konsep_ct_temp.id_konsep_ct JOIN master_soal_ct
                                    on konsep_soal.id_soal_ct = master_soal_ct.id_soal_ct JOIN kategori_level_soal
                                    on konsep_soal.id_soal_ct = kategori_level_soal.id_soal_ct JOIN master_kategori_umur
                                    on kategori_level_soal.id_kategori_umur = master_kategori_umur.id_kategori_umur ";
      
      //menampung query select apabila user tida memilih kategori pencarian konsep CT
      $query_konsep_false = "SELECT DISTINCT konsep_soal.id_soal_ct, 
                                            master_soal_ct.judul_soal, 
                                            master_soal_ct.tipe_soal,
                                            count(konsep_soal.id_konsep_ct) AS jumlah
                                            FROM konsep_soal JOIN master_soal_ct
                                              on konsep_soal.id_soal_ct = master_soal_ct.id_soal_ct JOIN kategori_level_soal
                                              on konsep_soal.id_soal_ct = kategori_level_soal.id_soal_ct JOIN master_kategori_umur
                                              on kategori_level_soal.id_kategori_umur = master_kategori_umur.id_kategori_umur ";
      

      
      
      //Seleksi soal where TipeSoal = true, Jenjang Pendidikan = True, Berdasarkan Umur = false
      $query2=" WHERE master_soal_ct.tipe_soal IN ('$tipeSoal') AND
                (master_kategori_umur.umur_awal BETWEEN (SELECT master_kategori_umur.umur_awal
                                                        FROM master_kategori_umur
                                                        WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                        ORDER BY master_kategori_umur.umur_awal asc
                                                        LIMIT 1)
                                                AND
                                                    (SELECT master_kategori_umur.umur_akhir
                                                      FROM master_kategori_umur
                                                      WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                      ORDER BY master_kategori_umur.umur_akhir desc
                                                      LIMIT 1) 
                AND master_kategori_umur.umur_akhir BETWEEN (SELECT master_kategori_umur.umur_awal
                                                              FROM master_kategori_umur
                                                              WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                              ORDER BY master_kategori_umur.umur_awal asc
                                                              LIMIT 1)
                                                    AND
                                                      (SELECT master_kategori_umur.umur_akhir
                                                        FROM master_kategori_umur
                                                        WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                        ORDER BY master_kategori_umur.umur_akhir desc 
                                                        LIMIT 1)
                 )
                GROUP BY konsep_soal.id_soal_ct,
                master_kategori_umur.nama_kategori,  
                kategori_level_soal.id_level_soal
                ORDER BY jumlah desc, konsep_soal.id_soal_ct";

      
      //Seleksi soal where TipeSoal = true, Jenjang Pendidikan = false, Berdasarkan Umur = true
      $query3=" WHERE master_soal_ct.tipe_soal IN ('$tipeSoal') AND
                ((master_kategori_umur.umur_awal >= $umurAwal AND master_kategori_umur.umur_akhir <= $umurAkhir) OR 
                master_kategori_umur.umur_awal BETWEEN $umurAwal AND $umurAkhir OR
                master_kategori_umur.umur_akhir BETWEEN $umurAwal AND $umurAkhir)
                GROUP BY konsep_soal.id_soal_ct,
                         master_kategori_umur.nama_kategori,  
                kategori_level_soal.id_level_soal
                ORDER BY jumlah desc, konsep_soal.id_soal_ct";
        
      
      //Seleksi soal where TipeSoal = false, Jenjang Pendidikan = True, Berdasarkan Umur = false
      $query6="  WHERE (master_kategori_umur.umur_awal BETWEEN (SELECT master_kategori_umur.umur_awal
                                                          FROM master_kategori_umur
                                                          WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                          ORDER BY master_kategori_umur.umur_awal asc
                                                          LIMIT 1)
                                                  AND
                                                      (SELECT master_kategori_umur.umur_akhir
                                                        FROM master_kategori_umur
                                                        WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                        ORDER BY master_kategori_umur.umur_akhir desc
                                                        LIMIT 1) 
                  AND 
                  master_kategori_umur.umur_akhir BETWEEN (SELECT master_kategori_umur.umur_awal
                                                            FROM master_kategori_umur
                                                            WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                            ORDER BY master_kategori_umur.umur_awal asc
                                                            LIMIT 1)
                                                  AND
                                                      (SELECT master_kategori_umur.umur_akhir
                                                        FROM master_kategori_umur
                                                        WHERE master_kategori_umur.id_kategori_umur IN ($jenjangPendidikan)
                                                        ORDER BY master_kategori_umur.umur_akhir desc 
                                                        LIMIT 1)
                 )
                GROUP BY konsep_soal.id_soal_ct,
                master_kategori_umur.nama_kategori,  
                kategori_level_soal.id_level_soal
                ORDER BY jumlah desc, konsep_soal.id_soal_ct";

      //Seleksi soal where TipeSoal = false, Jenjang Pendidikan = false, Berdasarkan Umur = true
      $query7=" WHERE((master_kategori_umur.umur_awal >= $umurAwal AND master_kategori_umur.umur_akhir <= $umurAkhir) OR 
                master_kategori_umur.umur_awal BETWEEN $umurAwal AND $umurAkhir OR
                master_kategori_umur.umur_akhir BETWEEN $umurAwal AND $umurAkhir)
                GROUP BY konsep_soal.id_soal_ct,
                master_kategori_umur.nama_kategori,  
                kategori_level_soal.id_level_soal
                ORDER BY jumlah desc, konsep_soal.id_soal_ct"; 

      //Seleksi soal default
       $query9=" GROUP BY konsep_soal.id_soal_ct,
                master_kategori_umur.nama_kategori,  
                kategori_level_soal.id_level_soal
                ORDER BY konsep_soal.id_soal_ct desc"; 


  if($konsepCT_cek == 0 && $tipeSoal_cek == 0 && $jenjangPendidikan_cek == 0 && $umurAwal_cek == "false"){   
      $query = $this->db->query("$query_konsep_false".''."$query9"); 
  }
  else{
    if($konsepCT_cek != 0){    
            
          if($tipeSoal_cek != 0){
              
                if($jenjangPendidikan_cek != 0){

                         
                      $query = $this->db->query("$query_konsep_true".''."$query2");                 
                                                
                }
                else{

                     
                      $query = $this->db->query("$query_konsep_true".''."$query3");

                }
          }
          else{
              if($jenjangPendidikan_cek != 0){

                    
                      $query = $this->db->query("$query_konsep_true".''."$query6");
              }
              else{
              
                      
                      $query = $this->db->query("$query_konsep_true".''."$query7");
                  
              } 
          }
      }
      
      else{
          if($tipeSoal_cek != 0){
                
            if($jenjangPendidikan_cek != 0){
                   
                    $query = $this->db->query("$query_konsep_false".''."$query2");
            }
            else{
                    
                    $query = $this->db->query("$query_konsep_false".''."$query3");
            }       
        }
        else{
            if($jenjangPendidikan_cek != 0){
                    
                    $query = $this->db->query("$query_konsep_false".''."$query6");
            }
            else{
        
                    $query = $this->db->query("$query_konsep_false".''."$query7");
            } 
        }
    }  
  
  } 

  $res =  $query->result();
 
  
  if(!empty($res)){
    
    $this->session->unset_userdata('soal_tidak_ditemukan');
    return $res;
  }
  else{
    $this->session->set_flashdata('soal_tidak_ditemukan', 'Maaf, soal tidak ditemukan : silahkan cari soal dengan kriteria lain');
  }
 


}

     function insert_konsep_temp($id_konsep_ct){
        $query = $this->db->query(" INSERT INTO konsep_ct_temp (id_konsep_ct) VALUES ('$id_konsep_ct')");
     }

    function insert_soal_relevan_temp($id_soal_ct,$judul_soal,$tipe_soal,$jumlah_konsep){
      $query = $this->db->query(" INSERT INTO soal_relevan_temp (id_soal_ct,judul_soal,tipe_soal,jumlah_konsep) 
                                  VALUES ('$id_soal_ct',".$this->db->escape($judul_soal).",'$tipe_soal','$jumlah_konsep')
                                ");
    }

    function delete_konsep_temp(){
        $query = $this->db->query("TRUNCATE konsep_ct_temp");
    }

    function delete_soal_relevan_temp(){
      $query = $this->db->query("TRUNCATE soal_relevan_temp");
    }

   function jumlah_data_soal(){
		 return $this->db->get('soal_relevan_temp')->num_rows();
   }

    function get_data_soal($number,$offset){
      return $query = $this->db->get('soal_relevan_temp',$number,$offset)->result();		
    }
     

  }