<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
	<style>
		 .demoInputBox {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: auto;
            }
	</style>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

			<!--session-->
				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_kategori_umur')): ?>
				<div class="alert alert-danger" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_kategori_umur'); ?>
				</div>
				<?php endif; ?>
				

				<br>	
				<h5 class="font-weight-bold text-center">Sunting Data Kategori Umur</h5>
				<br>


				
				<!-- Card  -->
				<div class="card mb-3">	
					<div class="card-header">

						<a href="<?php echo site_url('admin/kategori_umur_ct/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/kategori_umur_ct/edit') ?>" method="post">

							<input type="hidden" name="id_kategori_umur" value="<?php echo $kategori_umur_id->id_kategori_umur?>" />

							<div class="form-group">
								<label for="nama_kategori">Nama Kategori Umur*</label>
								<input class="form-control <?php echo form_error('nama_kategori') ? 'is-invalid':'' ?>"
								 type="text" name="nama_kategori" value="<?php echo htmlspecialchars($kategori_umur_id->nama_kategori)?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('nama_kategori') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_negara">Negara*</label>							
								<div>
									<select name="id_negara" id="id_negara" class="demoInputBox">
										<?php
											foreach ($negara as $i) {
													$id_negara_selected = $kategori_umur_id->id_negara;
													$id_negara = $i->id_negara;
													if($id_negara_selected == $id_negara){
														echo '<option value='.$i->id_negara.' selected>'.$i->nama_negara.'</option>';
													}
													else{
														echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
													}
												}
										?>
									</select>
								</div>
							</div>

							<div class="form-group" style="width:auto;" >
								<label for="umurAwal">Umur Awal : </label>
								<div>
									<select id="umurAwal" name="umurAwal" class="demoInputBox">
										<option value="Pilih Umur Awal" disabled>Pilih Umur Awal : </option>
										<?php
													for($i = 6 ; $i < 18 ; $i++){
														if($i == $kategori_umur_id->umur_awal){
														echo '<option value='.$i.' selected>'.$i.' tahun </option>';
														}
														else{
														echo '<option value='.$i.'>'.$i.' tahun </option>';
														}
													}
											?>
									</select> 
								</div> 
         				    </div>

							 <div class="form-group" style="width:auto;" >
								<label for="umurAkhir">Umur Akhir : </label>
								<div>
									<select id="umurAkhir" name="umurAkhir" class="demoInputBox">  
										<option value="Pilih Umur Akhir" disabled>Pilih Umur Akhir :  </option> 
													<?php
														    for($i = 7 ; $i <= 18 ; $i++){
																 if($i == $kategori_umur_id->umur_akhir){
																	echo '<option value='.$i.' selected>'.$i.' tahun </option>';
																 }
																 else{
																	echo '<option value='.$i.'>'.$i.' tahun </option>';
																 }
															 }
														?>
									</select>							
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>	
		$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});


			$("#umurAwal").change(function(){
        
				for(i=7; i<=18; i++) {
					$("#umurAkhir option[value='" + i +"']").remove();
				}
	
				var val = parseInt($("#umurAwal").val());
				
				for(i=val+1; i<=18; i++) {
							$("#umurAkhir").append("<option value='" + i + "'>"+i+" tahun </option>");
				}
	
				if(val == 18){
					$("#umurAkhir").append("<option value='" + val + "'>"+val+" tahun </option>");
				}
	
					$("#jenjangPendidikan6").prop("disabled", true);
					$("#jenjangPendidikan7").prop("disabled", true);
					$("#jenjangPendidikan8").prop("disabled", true);
	
	
		});

			
		});

	
</script>

</html>