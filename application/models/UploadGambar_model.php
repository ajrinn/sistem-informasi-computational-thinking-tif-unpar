<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UploadGambar_model extends CI_model{

    function __construct(){
        parent::__construct();
    }

    function insert_gambar($image_content,$tipe_gambar,$keterangan_gambar){
        $query = $this->db->query("INSERT INTO master_image_ct (image_content,tipe_gambar,keterangan_gambar) 
                                   values ('$image_content','$tipe_gambar','$keterangan_gambar')");
        if($query !== NULL){
            return TRUE;
        }
        return FALSE;
        
    }

    function insert_gambar_liputan($image_content,$keterangan_gambar){

        $query = $this->db->query("INSERT INTO master_image_lip (image_content,keterangan_gambar) values ('$image_content','$keterangan_gambar')");
        if($query !== NULL){
            return TRUE;
        }
        return FALSE;
        
    }

    function display_gambar($id_gambar){
        $query = $this->db->query("SELECT * FROM master_image_ct where id_image='$id_gambar'");
        
        foreach( $query->result_array() as $key){
           $image = $key['image_content']; 
           //echo '<img src="data:image/jpeg;base64,'.$key['image_content'].'"/>';
        }

        return $image;
    }

    function display_gambar_liputan($id_gambar){
        $query = $this->db->query("SELECT * FROM master_image_lip where id_image='$id_gambar'");
        
        foreach( $query->result_array() as $key){
           $image = $key['image_content']; 
           //echo '<img src="data:image/jpeg;base64,'.$key['image_content'].'"/>';
        }
        
        return $image;
    }

    function get_first_imageSoal($id_soal_ct){

        $query = $this->db->query(
                "SELECT master_image_ct.id_image
                FROM master_image_ct JOIN image_soal
                on master_image_ct.id_image = image_soal.id_image 
                JOIN master_soal_ct 
                on master_soal_ct.id_soal_ct = image_soal.id_soal_ct
                WHERE master_soal_ct.id_soal_ct = '$id_soal_ct' 
                ORDER BY master_image_ct.id_image
                LIMIT 1;");
        $res = $query->row();

        if(!empty($res)){
         $temp =  $res->id_image;
         
        }
        else{
          $temp = false;
        }

        return $temp;

    }

    function get_first_imagePembahasan($id_soal_ct){

        $query = $this->db->query(
                "SELECT master_image_ct.id_image
                FROM master_image_ct JOIN image_pembahasan
                on master_image_ct.id_image = image_pembahasan.id_image 
                WHERE image_pembahasan.id_pembahasan = '$id_soal_ct' 
                ORDER BY master_image_ct.id_image
                LIMIT 1;");
        $res = $query->row();

        if(!empty($res)){
         $temp =  $res->id_image;
         
        }
        else{
          $temp = false;
        }

        return $temp;

    }


    function get_first_imageLiputan($id_liputan){

        $query = $this->db->query(
                "SELECT master_image_lip.id_image
                FROM master_image_lip JOIN image_liputan
                on master_image_lip.id_image = image_liputan.id_image 
                JOIN master_liputan_ct 
                on master_liputan_ct.id_liputan = image_liputan.id_liputan
                WHERE master_liputan_ct.id_liputan = '$id_liputan' 
                ORDER BY master_image_lip.id_image
                LIMIT 1;");
        $res = $query->row();

        if(!empty($res)){
         $temp =  $res->id_image;
         
        }
        else{
          $temp = false;
        }

        return $temp;

    }

    function get_imageSoal($id_soal_ct){
        $query = $this->db->query(
                "SELECT master_image_ct.id_image, master_image_ct.tipe_gambar, master_image_ct.keterangan_gambar
                FROM master_image_ct JOIN image_soal
                on master_image_ct.id_image = image_soal.id_image
                WHERE image_soal.id_soal_ct = '$id_soal_ct';");
        $res = $query->result();
        return $res;
    }

    function get_imagePembahasan($id_soal_ct){
        $query = $this->db->query(
                "SELECT master_image_ct.id_image, master_image_ct.tipe_gambar,master_image_ct.keterangan_gambar
                FROM master_image_ct JOIN image_pembahasan
                on master_image_ct.id_image = image_pembahasan.id_image
                WHERE image_pembahasan.id_pembahasan = '$id_soal_ct';");
        $res = $query->result();
        return $res;
    }

    function get_id_imageLiputan($id_liputan){

        $query = $this->db->query(
                "SELECT master_image_lip.id_image,master_image_lip.keterangan_gambar
                FROM master_image_lip JOIN image_liputan
                on master_image_lip.id_image = image_liputan.id_image  JOIN master_liputan_ct
                on master_liputan_ct.id_liputan = image_liputan.id_liputan
                WHERE master_liputan_ct.id_liputan = '$id_liputan';");
        $res = $query->result();
        return $res;

    }

    function get_id_imageSoalDeskripsi($id_soal_ct){

        $query = $this->db->query(
                "SELECT master_image_ct.id_image,master_image_ct.tipe_gambar,master_image_ct.keterangan_gambar
                FROM master_image_ct JOIN image_soal
                on master_image_ct.id_image = image_soal.id_image 
                JOIN master_soal_ct 
                on master_soal_ct.id_soal_ct = image_soal.id_soal_ct
                WHERE master_soal_ct.id_soal_ct = '$id_soal_ct' AND master_image_ct.tipe_gambar = 'Soal (Deskripsi Soal)';");
        $res = $query->result();
        return $res;

    }

    function get_id_imageSoalPertanyaan($id_soal_ct){

        $query = $this->db->query(
                "SELECT master_image_ct.id_image,master_image_ct.tipe_gambar,master_image_ct.keterangan_gambar
                FROM master_image_ct JOIN image_soal
                on master_image_ct.id_image = image_soal.id_image 
                JOIN master_soal_ct 
                on master_soal_ct.id_soal_ct = image_soal.id_soal_ct
                WHERE master_soal_ct.id_soal_ct = '$id_soal_ct' AND master_image_ct.tipe_gambar = 'Soal (Pertanyaan)';");
        $res = $query->result();
       
        return $res;

    }

    function get_id_imagePembahasanJawaban($id_soal_ct){

        $query = $this->db->query(
                "SELECT master_image_ct.id_image,master_image_ct.tipe_gambar,master_image_ct.keterangan_gambar
                FROM master_image_ct JOIN image_pembahasan
                on master_image_ct.id_image = image_pembahasan.id_image 
                JOIN master_pembahasan_soal_ct 
                on master_pembahasan_soal_ct.id_pembahasan = image_pembahasan.id_pembahasan
                JOIN master_soal_ct
                on master_soal_ct.id_soal_ct = master_pembahasan_soal_ct.id_soal_ct
                WHERE master_pembahasan_soal_ct.id_soal_ct = '$id_soal_ct' AND master_image_ct.tipe_gambar = 'Pembahasan (Jawaban)';");
        $res = $query->result();
        return $res;

    }

    function get_id_imagePembahasanPenjelasan($id_soal_ct){

        $query = $this->db->query(
            "SELECT master_image_ct.id_image,master_image_ct.tipe_gambar,master_image_ct.keterangan_gambar
            FROM master_image_ct JOIN image_pembahasan
            on master_image_ct.id_image = image_pembahasan.id_image 
            JOIN master_pembahasan_soal_ct 
            on master_pembahasan_soal_ct.id_pembahasan = image_pembahasan.id_pembahasan
            JOIN master_soal_ct
            on master_soal_ct.id_soal_ct = master_pembahasan_soal_ct.id_soal_ct
            WHERE master_pembahasan_soal_ct.id_soal_ct = '$id_soal_ct' AND master_image_ct.tipe_gambar = 'Pembahasan (Penjelasan Jawaban)';");
        $res = $query->result();
        return $res;

    }


    //untuk mendapatkaan last insert id gambar dari tabel master_image_ct dan tabel master_image_lip   

    public function getLastImageID(){
        $query = $this->db->query("SELECT id_image FROM master_image_ct ORDER BY id_image DESC LIMIT 1");
        $id = $query->row()->id_image;
        return $id;
    }

    public function getLastImageLiputanID(){
        $query = $this->db->query("SELECT id_image FROM master_image_lip ORDER BY id_image DESC LIMIT 1");
        $id = $query->row()->id_image;
        return $id;
    }


    //untuk melakukan update gambar pada tabel master_image_ct dan tabel master_liputan_ct

    public function update_image_soal($id_image,$image_content, $tipe_gambar, $keterangan_gambar){
        $query = $this->db->query("UPDATE master_image_ct 
                                    SET image_content = '$image_content',  
                                        tipe_gambar = '$tipe_gambar',
                                        keterangan_gambar = '$keterangan_gambar' 
                                    WHERE id_image = '$id_image'"
                                );
        return $query;
    }

    public function update_image_liputan($id_image,$image_content,$keterangan_gambar){
        $query = $this->db->query("UPDATE master_image_lip
                                    SET image_content = '$image_content',  
                                        keterangan_gambar = '$keterangan_gambar' 
                                    WHERE id_image = '$id_image'"
                                );
        return $query;
    }


    //untuk menghapus record pada tabel transaksi
    public function delete_imageSoal($id){
        $this->db->query("
                            DELETE master_image_ct, image_soal
                            FROM master_image_ct JOIN image_soal  
                             on image_soal.id_image = master_image_ct.id_image 
                            where image_soal.id_soal_ct = '$id';

                        ");
        $this->resetAI();
    }

    public function delete_imagePembahasan($id){
        $this->db->query("
                            DELETE master_image_ct, image_pembahasan
                            FROM master_image_ct JOIN image_pembahasan  
                             on image_pembahasan.id_image = master_image_ct.id_image 
                            where image_pembahasan.id_pembahasan = '$id';
                        ");
        $this->resetAI();
    }

    public function delete_imageLiputan($id){
        $this->db->query("
                            DELETE master_image_lip, image_liputan
                            FROM master_image_lip JOIN image_liputan 
                             on image_liputan.id_image = master_image_lip.id_image 
                            where image_liputan.id_liputan = '$id';
                        ");
        $this->resetAI_liputan();
    }

    //untuk men-set AUTO_INCREMENT agar mulai dari 1
    public function resetAI(){
        $this->db->query("ALTER TABLE master_image_ct AUTO_INCREMENT=1;");
    }

    public function resetAI_liputan(){
        $this->db->query("ALTER TABLE master_image_lip AUTO_INCREMENT=1;");
    }


    //menghapus gambar soal, gambar pembahasan, dan gambar liputan
    function delete_image($id_image){
        $res = 0;

        $query = $this->db->query("
            SELECT image_soal.id_soal_ct 
            FROM image_soal WHERE 
            image_soal.id_image = $id_image 
        ");
        $res = $query->row()->id_soal_ct;
        print_r($res);
        
        $this->db->query("
                            DELETE master_image_ct
                            FROM master_image_ct 
                            where master_image_ct.id_image = $id_image;
        ");
        return $res;
    }



    function delete_image_pembahasan($id_image){
        //echo $id_image;
        $query = $this->db->query("
            SELECT image_pembahasan.id_pembahasan 
            FROM image_pembahasan WHERE 
            image_pembahasan.id_image = '$id_image' 
        ");
        $res = $query->row()->id_pembahasan;
        print_r($res);
        
        $this->db->query("
                            DELETE master_image_ct
                            FROM master_image_ct 
                            where master_image_ct.id_image = '$id_image';
        ");

        return $res;
    }

    function delete_image_liputan($id_image){
    
        $query = $this->db->query("
            SELECT image_liputan.id_liputan 
            FROM image_liputan WHERE 
            image_liputan.id_image = '$id_image' 
        ");

        
        $res = $query->row()->id_liputan;
        
        $this->db->query("
                            DELETE master_image_lip
                            FROM master_image_lip 
                            where master_image_lip.id_image = '$id_image';
        ");

        return $res;
    }


}
