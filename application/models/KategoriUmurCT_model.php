<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriUmurCT_model extends CI_Model {

    private $_table = "master_kategori_umur";

    public function __construct(){
      parent::__construct();   
      $this->load->model("Negara_model");
   }

    public function getAll(){

          //return $this->db->get($this->_table)->result();
         //Artinya seperti select * from master_konsep_ct

         $query = $this->db->query("SELECT *
                                   FROM master_kategori_umur JOIN master_negara
                                   ON master_kategori_umur.id_negara = master_negara.id_negara;
                                  ");
        $res = $query->result();
        return $res;
    }

    public function getById($id){
        $query = $this->db->query("
                                    SELECT *
                                    FROM master_kategori_umur JOIN master_negara
                                    ON master_kategori_umur.id_negara = master_negara.id_negara
                                    where master_kategori_umur.id_kategori_umur = '$id';
        ");
        $res = $query->row();
        return $res;
      }
  
      public function validate_form(){
        $this->form_validation->set_rules('nama_kategori','Nama Kategori Umur','required');
        
        $validation = $this->form_validation->run();
        if($validation == TRUE){
          return TRUE;
        }
        else{
          return FALSE;
        }
  
      }
  
      public function save(){
  
        $post = $this->input->post(); 
        $nama_kategori = $post["nama_kategori"];
        $id_negara = $post["id_negara"];
        $umur_awal = $post["umurAwal"];
        $umur_akhir =  $post["umurAkhir"];

        $data_kategoriUmur = $this->getAll();
        $cek = true;

        foreach($data_kategoriUmur as $i){
           $nama_temp = $i->nama_kategori;
           $awal_temp = $i->umur_awal;
           $akhir_temp = $i->umur_akhir;
           $id_negara_temp = $i->id_negara;
           
           if($nama_kategori == $nama_temp || ($umur_awal == $awal_temp && $umur_akhir == $akhir_temp)){
              if($id_negara == $id_negara_temp){
                $cek = false;
                break;
              }
           }
        }

        if($cek == true){
          $query = $this->db->query("
                                    INSERT INTO master_kategori_umur(nama_kategori,id_negara,umur_awal,umur_akhir)
                                    VALUES ('$nama_kategori',$id_negara,$umur_awal,$umur_akhir);
          ");
          
          return true;
        }
        else{
          return false;
        }
  
    }
  
      public function update(){
          $post = $this->input->post();
          $id_kategori_umur = $post["id_kategori_umur"];
          $nama_kategori = $post["nama_kategori"];
          $id_negara = $post["id_negara"];
          $umur_awal = $post["umurAwal"];
          $umur_akhir =  $post["umurAkhir"];

          $data_kategoriUmur = $this->getAll();
          $cek = true;
  
          foreach($data_kategoriUmur as $i){
            $id_temp = $i->id_kategori_umur;
             $id_negara_temp = $i->id_negara;
             $nama_temp = $i->nama_kategori;
             $awal_temp = $i->umur_awal;
             $akhir_temp = $i->umur_akhir;
             
             //echo $umur_awal." ";
             //echo $awal_temp." -";

             if( $id_kategori_umur != $id_temp){
                if($nama_kategori == $nama_temp || ($umur_awal == $awal_temp && $umur_akhir == $akhir_temp)){
                  if($id_negara == $id_negara_temp){
                    $cek = false;
                    break;
                  }
                }
             }
              
          }
          
          if($cek == true){
              $query = $this->db->query("
                UPDATE master_kategori_umur
                SET nama_kategori = ".$this->db->escape($nama_kategori).",
                    id_negara = '$id_negara',
                    umur_awal = '$umur_awal',
                    umur_akhir = '$umur_akhir'
                WHERE id_kategori_umur = '$id_kategori_umur'
              ");
              return true;
          }
          else{
             return false;
          }
  
      }
  
      public function delete($id){
        //method ini menghapus data pada tabel berdasarkan id
        return $this->db->delete($this->_table,array("id_kategori_umur" => $id));
    }

    function get_jenjang_pendidikan(){
        $query = $this->db->query("
                                    SELECT * 
                                    FROM master_kategori_umur JOIN master_negara
                                    ON master_kategori_umur.id_negara = master_negara.id_negara
                                    WHERE  master_negara.nama_negara = 'Indonesia';
        ");
        $res = $query->result();
        return $res;
    }

    function jumlah_data_kategoriUmur(){
      return $this->db->get('master_kategori_umur')->num_rows();
    }
      
    function get_data_kategoriUmur($number,$offset){
      $query = $this->db->query(" SELECT *
                                  FROM master_kategori_umur JOIN master_negara
                                  ON master_kategori_umur.id_negara = master_negara.id_negara
                                  ORDER BY master_kategori_umur.id_kategori_umur desc
                                  LIMIT $number
                                  OFFSET $offset;
                                ");
      $res = $query->result();
      return $res;		
    }


}