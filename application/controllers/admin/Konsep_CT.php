<?php
defined('BASEPATH') OR exit('No direct script allowed');

class Konsep_CT extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("KonsepCT_model"); //meload admin_model
        $this->load->library('form_validation'); //meload library form_validation 
        $this->load->library('pagination'); // meload library pagination
    }



    public function index(){
        $data["konsep_ct"] = $this->KonsepCT_model->getAll();

        $config['base_url'] = base_url("index.php/admin/konsep_ct/index/");
        $config['total_rows'] = $this->KonsepCT_model->jumlah_data_konsep();
        $config['per_page'] = $this->KonsepCT_model->jumlah_data_konsep();
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        

        $this->pagination->initialize($config);	
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["data_konsep"] = $this->KonsepCT_model->get_data_konsep($config['per_page'],$data['page']);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view("admin\data_properti_ct\data_konsep_ct\list_konsep", $data);

    }

    public function add(){


        $konsep_ct = $this->KonsepCT_model; 
        $validation = $konsep_ct->validate_form();  

        if($validation == TRUE){ 
            
            if( $konsep_ct->save() == true){
                $this->session->set_flashdata('success','Berhasil disimpan'); 
                redirect('admin/konsep_ct/add'); 
            }
            else{
                $this->session->set_flashdata('gagal_konsep', 'Terjadi kesalahan : Konsep CT tidak boleh sama!');
            }

        }

        $this->load->view("admin/data_properti_ct/data_konsep_ct/new_form_konsep"); 

    }

    public function edit($id = null){

        
        if(!isset($id)){
            redirect('admin/konsep_ct'); 
        }

        $konsep_ct = $this->KonsepCT_model; 
        $validation = $konsep_ct->validate_form(); 

        if($validation == TRUE){ 
             
            if($konsep_ct->update() == true){
                $this->session->set_flashdata('success','Berhasil disimpan');
                redirect('admin/konsep_ct/edit/'.$id); 

            }
            else{
                $this->session->set_flashdata('gagal_konsep', 'Terjadi kesalahan : Konsep CT tidak boleh sama!');
            }

        }
        
        $data["konsep_ct_id"] = $konsep_ct->getById($id); 
        if(!$data["konsep_ct_id"]){                   
            show_404();
        }

        $this->load->view("admin/data_properti_ct/data_konsep_ct/edit_form_konsep",$data); 
    }


    public function delete($id = null){
        if(!isset($id)){
            show_404();
        }

        if($this->KonsepCT_model->delete($id)){
            redirect('admin/konsep_ct');
        }
    }

    

   

}