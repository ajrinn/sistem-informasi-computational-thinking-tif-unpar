<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

    <?php $this->load->view("admin/_partials/navbar.php") ?>

    <div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<br>
				<h5 class="text-center text-bold font-weight-bold">Daftar Soal CT</h5>
				<br>

				<div class="card mb-3">
				   
					<div class="card-header">
						<a href="<?php echo site_url('admin/soal_ct/add')?>"><i class="fas fa-plus"></i> Tambah Soal CT dan Pembahasan</a>
					</div>

					<div class="card-body">

					<?php if (!empty($data_soal)): ?>

						<div class="table-responsive">
							<table class="table table-bordered table-hover" id="table_soal" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="col-sm-1">No.</th>
                                        <th class="col-sm-3">Judul Soal</th>
										<th class="col-sm-3">Tipe Soal</th>
                                        <th class="col-sm-3">Negara Asal Soal</th>
										<th class="col-sm-1">Tahun</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$count = 1;
									foreach ($data_soal as $i): ?>
									<tr>
										<td>
											<?php 
												 if(($page > $count)){
													$count = $page+1;													 
												 }
												 echo $count;
											?>
										</td>
                                        <td>
											<?php echo $i->judul_soal?>
										</td>
                                        <td>
											<?php echo $i->tipe_soal?>
										</td>
                                        <td>
											<?php echo $i->nama_negara?>
										</td>
										<td>
											<?php echo $i->tahun?>
										</td>
										<td width="250">


											<a href="<?php echo base_url();?>index.php/admin/soal_ct/tampilkanDataSoal/<?php echo $i->id_soal_ct?>"
											class="btn text-primary"><i class="fas fa-info-circle"></i> Lihat Soal dan Pembahasan</a> <br>								

											<a href="<?php echo site_url('admin/soal_ct/edit/'.$i->id_soal_ct) ?>"
											class="btn btn-small text-warning"><i class="fas fa-edit"></i> Sunting Soal dan Pembahasan</a>
											
											<!--<a href="<?php //echo site_url('admin/pembahasan_soal_ct/edit/'.$i->id_pembahasan) ?>"
								            class="btn btn-small text-warning"><i class="fas fa-edit"></i> Edit Pembahasan</a>-->

											<a onclick="deleteConfirm('<?php echo site_url('admin/soal_ct/delete/'.$i->id_soal_ct) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>

										</td>
									</tr>
									<?php $count=$count+1; endforeach; ?>

								</tbody>
							</table>
						</div>

						<div class="row">
							<div class="col">
							<?php echo $pagination;?>
							</div>
						</div>

						<?php endif; ?>
					</div>

					<!-- Modal -->
				   
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php //$this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
		$('#btn-delete').attr('href',url);
		$('#deleteModal').modal();
		}




		$(document).ready(function(){

			$('#table_soal').DataTable({
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 5 ],
					orderData: [ 5, 0 ]
				} ]
			});

			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
			

		});



	</script>

</body>

</html>