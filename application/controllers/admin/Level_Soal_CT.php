<?php
defined('BASEPATH') OR exit('No direct script allowed');

class Level_Soal_CT extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("LevelSoalCT_model"); //meload admin_model
        $this->load->library('form_validation'); //meload library form_validation 
        $this->load->library('pagination');             // meload library pagination
    }



    public function index(){
        $data["level_soal_ct"] = $this->LevelSoalCT_model->getAll();

        $config['base_url'] = base_url("index.php/admin/level_soal_ct/index/");
        $config['total_rows'] = $this->LevelSoalCT_model->jumlah_data_level();
        $config['per_page'] = $this->LevelSoalCT_model->jumlah_data_level();
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        

        $this->pagination->initialize($config);	
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["data_level"] = $this->LevelSoalCT_model->get_data_level($config['per_page'],$data['page']);
		$data['pagination'] = $this->pagination->create_links();

        $this->load->view("admin\data_properti_ct\data_level_soal\list_level", $data);

    }

    public function add(){


        $level_soal_ct = $this->LevelSoalCT_model; 
        $validation = $level_soal_ct->validate_form();  

        if($validation == TRUE){ 
            
            if($level_soal_ct->save() == true){
                $this->session->set_flashdata('success','Berhasil disimpan');
                redirect('admin/level_soal_ct/add'); 
            }
            else{
                $this->session->set_flashdata('gagal_level', 'Terjadi kesalahan : Level soal dan keterangan tidak boleh sama!');
            }
        }

        $this->load->view("admin/data_properti_ct/data_level_soal/new_form_level"); 

    }

    public function edit($id = null){

        
        if(!isset($id)){
            redirect('admin/level_soal_ct'); 
        }

        $level_soal_ct = $this->LevelSoalCT_model; 
        $validation = $level_soal_ct->validate_form();  

        if($validation == TRUE){ 
           
            if($level_soal_ct->update() == true){
                $this->session->set_flashdata('success','Berhasil disimpan');
                redirect('admin/level_soal_ct/edit/'.$id); 

            }else{
                $this->session->set_flashdata('gagal_level', 'Terjadi kesalahan : Level soal dan keterangan tidak boleh sama!');
            }

        }
        
        $data["level_ct_id"] = $level_soal_ct->getById($id); 
        if(!$data["level_ct_id"]){                   
            show_404();
        }

        $this->load->view("admin/data_properti_ct/data_level_soal/edit_form_level",$data); 
    }


    public function delete($id = null){
        if(!isset($id)){
            show_404();
        }

        if($this->LevelSoalCT_model->delete($id)){
            redirect('admin/level_soal_ct');
        }
    }



    

   

}