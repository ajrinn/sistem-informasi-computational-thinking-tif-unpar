<?php
defined('BASEPATH') OR exit('No direct script allowed');

class Login extends CI_Controller{

    public function __construct(){
       parent::__construct();

       $this->load->model("admin_model");

       $this->load->library('form_validation'); //meload library form_validation
    }

    function index(){

        if(isset($_POST['submit'])){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $berhasil = $this->admin_model->cek_login($username,$password);
            
           
              if($berhasil == 1){
                 $this->session->set_flashdata('success_login', 'Selamat anda telah berhasil login');
                  redirect('admin/admin');
              }
              else{
                $this->session->set_flashdata('error', 'Username atau password salah !');
                redirect('admin/login');
                
              }
          }
          else{
            //$this->session->set_flashdata('error', 'Error Message...');
             $this->load->view('admin/data_admin/login');
          }
    }

    
    function logout(){
        $this->session->sess_destroy();
        redirect('admin/login');
    }
}

?>