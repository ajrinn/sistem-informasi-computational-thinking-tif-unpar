<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    <a class="navbar-brand " href="<?php echo site_url('user/home')?>">Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>

<div class="contact_container">
<img src="<?php echo base_url(); ?>/assets/images/kontak.jpg" class="img-fluid" style="width:105%;height:450px">
  
</div>



<section id="contact" class="parallax-section">
     <div class="container" style="margin-bottom:0px">

     <?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert" style = "margin-top:20px">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
    <?php endif; ?>

          <div class="row">

               <div class="col-md-5 col-sm-8">
                    <!-- CONTACT INFO -->
                    <div class="wow fadeInUp contact-info" data-wow-delay="0.3s">
                        
                         <div class="section-title">
                        

                         <div class="container" style = "margin-top:10px">
	                             <div class="fluid">
                                    <div class="span8">
            	                            <iframe style="float:right;margin-top:50px" width="60%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-6.874931,107.606526&amp;q=Informatika%20UNPAR+(Informatika%20UNPAR)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"></iframe>
        	                          </div>
                                                                       
                                    <div style="float:left"> 
                                    <h3>Contact Info</h3>
                                    <p>Program Studi Informatika <br>
                                      Fakultas Teknologi Informasi dan Sains <br>
                                      <i class="fas fa-building"></i>  Gedung 9 Jalan Ciumbuleuit 94, Bandung 40141 Indonesia <br>
                                      <i class="fa fa-phone"></i>  ph. (022) 2032655, 2042004 ext. 100114 <br>
                                      <i class="fas fa-fax"></i> Fax. (022) 2031110 </p>

                                      <div style="margin-bottom:30px">
                                          <h3>Contact Us</h3>
                                          <form method="post" action="<?=base_url('index.php/user/contact/send')?>" enctype="multipart/form-data">
                                              <input type="text" class="form-control" id="nama" name="nama" placeholder="Name" required="required">
                                              <br>
                                              <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                                              <br>
                                              <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required="required">
                                              <br>
                                              <textarea rows="6" class="form-control" id="message" name="message" placeholder="Type your message here" required="required"></textarea>
                                              <br>
                                              <input type="submit" value="Send" class="btn btn-primary btn-block"/>
                                          </form>
                                      </div>

                                    </div>


                               </div>
                         </div>
                            

                              
                                 
                         </div>
                    </div>

                    
               </div>

               

          </div>

     </div>
</section>




<?php $this->load->view("user/_partials/footer.php") ?>

<script>
$( '#topheader .navbar-nav a' ).on( 'click', function () {
	$( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
	$( this ).parent( 'li' ).addClass( 'active' );
});
</script>