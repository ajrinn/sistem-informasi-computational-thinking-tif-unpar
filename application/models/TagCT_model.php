<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TagCT_model extends CI_Model{

    private $_table = "master_tags";

    public function getAll(){

      //Artinya seperti select * from master_tags
        $query = $this->db->query("
              SELECT * 
              FROM master_tags
              ORDER BY  master_tags.tag asc;
        ");  

        return $query->result();
     }

     public function getById($id){
      $query = $this->db->query("
                                  SELECT *
                                  from master_tags
                                  where master_tags.id_tag = $id;
      ");
      $res = $query->row();
      return $res;
    }

    public function validate_form(){
      $this->form_validation->set_rules('tag','Konsep CT','required');
      
      $validation = $this->form_validation->run();
      if($validation == TRUE){
        return TRUE;
      }
      else{
        return FALSE;
      }

    }

    public function save(){

      $post = $this->input->post(); 
      $tag = $post["tag"];
      $data_tag = $this->getAll();
      $cek = true;

      foreach( $data_tag as $i){
         $tag_temp = $i->tag;
         if($tag == $tag_temp){
            $cek = false;
            break;
         }  
      }

      if($cek == true){
        $query = $this->db->query("
                                  INSERT INTO master_tags(tag)
                                  VALUES (".$this->db->escape($tag).");
        ");
        return true;
      }
      else{
        return false;
      }

    }

    public function update(){
        $post = $this->input->post();
        $id_tag = $post["id_tag"];
        $tag = $post["tag"];
        $data_tag = $this->getAll();
        $cek = true;

      foreach( $data_tag as $i){
         $tag_temp = $i->tag;
         $id_temp = $i->id_tag;
         if($id_tag != $id_temp && $tag == $tag_temp){
            $cek = false;
            break;
         }  
      }

      if($cek == true){
          $query = $this->db->query("
            UPDATE master_tags
            SET tag = ".$this->db->escape($tag)."
            WHERE id_tag = $id_tag
          ");

          return true;
        }
      else{
          return false;
      }    

    }

    public function delete($id){
      //method ini menghapus data pada tabel berdasarkan id
      $this->db->query('alter table master_tags AUTO_INCREMENT=0');
      $this->db->delete($this->_table,array("id_tag" => $id));
   }

  function jumlah_data_tag(){
		return $this->db->get('master_tags')->num_rows();
  }
    
  function get_data_tag($number,$offset){
        $this->db->order_by("id_tag desc");
		    return $query = $this->db->get('master_tags',$number,$offset)->result();		
	}


}