<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    <a class="navbar-brand " href="<?php echo site_url('user/home')?>">Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>

<body>

<?php if (!empty($data_liputan_ct)): ?>   
  <div class="card" style="padding-bottom:30px">
    <div class="card-header">
      <a href="<?php echo site_url('user/liputan_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
    </div>

    <div class="card-header">
      <h5 class="text-center"> <?php echo $data_liputan_ct->judul_liputan?> </h5>

    </div>

      <?php
            foreach ($data_imageLiputan as $i){
                    $id_image = $i->id_image;
                    if($id_image != null){
                      echo '<img class="card-img img-responsive center-block" style="vertical-align:middle;width:60%;height:30%;padding:20px 0" src="data:image/jpeg;base64,'.$this->controller->display_gambar_liputan($id_image).'"/>';
                      echo '<p class="text-center">'.$i->keterangan_gambar.'</p>';
                    }
                    else{
                      echo '';
                    }
            }  
            
            $tgl = $data_liputan_ct->tanggal;
            $res = $this->controller->tanggal_indo(date('Y-m-d', strtotime($tgl)));
            echo '<p class="text-center">('.$res.') </p>';
        ?>
          
    <div class="card-body" style="padding-left:50px;padding-right:50px">
    <?php echo $data_liputan_ct->deskripsi_liputan?>
            
    </div>
</div>
<?php endif; ?>


<?php $this->load->view("user/_partials/footer.php") ?> 


</body>
</html>

