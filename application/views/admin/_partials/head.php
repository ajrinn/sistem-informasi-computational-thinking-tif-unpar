<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php echo SITE_NAME .": ". ucfirst($this->uri->segment(1)) . " - ". ucfirst($this->uri->segment(2)) ?></title>

    <!-- Bootstrap core CSS-->
    <!--<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <!--<link href="assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">-->
    <link href="<?php echo base_url('assets/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <!--<link href="assets/datatables/dataTables.bootstrap4.css" rel="stylesheet">-->
    <link href="<?php echo base_url('assets/datatables/dataTables.bootstrap4.css')?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <!--<link href="css/sb-admin.css" rel="stylesheet">-->
    <link href="<?php echo base_url('css/sb-admin.css')?>" rel="stylesheet">

    

    <script src="<?php echo base_url('assets/jquery/jquery-3.3.1.js')?>"></script>
    <script src="<?php echo base_url('assets/jquery/jquery.dataTables.min.js')?>"></script>
    <link href="<?php echo base_url('assets/css/jquery.dataTables.min.css')?>" red="stylesheet">

    <!--
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" rel="stylesheet">
    -->

    <script src="<?php echo base_url('assets/ckeditor/ckeditor.js') ?>"></script>

<style>


</style>