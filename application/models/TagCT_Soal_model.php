<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TagCT_Soal_model extends CI_Model{

    private $_table = "tags_soal";

    function insert_tag_soal ($id_soal_ct,$id_tag){
        $data = array('id_soal_ct' => $id_soal_ct, 'id_tag' => $id_tag);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;
    }

    public function update_tag_soal($id_soal_ct,$id_tag){
        $query = $this->db->query("UPDATE tags_soal SET id_tag = '$id_tag' WHERE id_soal_ct = '$id_soal_ct'");
        return $query;
    }

    public function delete_tag_soal($id_soal_ct){
        $query = $this->db->query("
                                    DELETE FROM tags_soal 
                                    WHERE tags_soal.id_soal_ct = '$id_soal_ct';
        ");
    }


    function getTag_by_idSoalCT($id_soal_ct){
        $query = $this->db->query("
                                    SELECT master_tags.tag
                                    FROM master_tags JOIN tags_soal
                                    on master_tags.id_tag = tags_soal.id_tag
                                    WHERE tags_soal.id_soal_ct = '$id_soal_ct';
        ");
        $res = $query->result();
        return $res;

    }

    function get_idTag_by_idSoalCT($id_soal_ct){
        $query = $this->db->query("
                                    SELECT tags_soal.id_tag
                                    FROM tags_soal
                                    WHERE tags_soal.id_soal_ct='$id_soal_ct';
        ");
        $res = $query->result();
        return $res;
    }

}