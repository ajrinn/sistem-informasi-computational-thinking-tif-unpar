<head>
    <style>
        img{
            max-width:600px;
        }

        .judul_soal{
            font-size:16px;
            text-align:center;
            font-weight: bold;
            font-family: serif;
        }

        p {
          font-family: serif;
        }

        .card-body1{
          border-style: solid;
          border-width: 1px;
        }

        .card-body2{
          border-style: solid;
          border-width: 1px;
        }

        .text-center1{
          border-style: solid;
          border-width: 1px;
        }

        span.header{
          display: inline-block;
        }

    </style>
</head>

<span class="header">
  <img src="assets/images/logo.jpg" style="width:8%;" />
</span>

<span class="header">
  <span style="font-size:20px">
        <strong>Computational Thinking TIF UNPAR</strong>
  </span>
  <br>
  <span style="font-size:15px">
    Program Studi Teknik Informatika UNPAR
  </span>
  <br>
  <span style="font-size:15px">
    Fakultas Teknologi Informasi dan Sains
  </span>
</span>


<?php foreach ($data_soal as $i): ?>


        <div class="card-header">  
          <h3 class="text-center1" style="text-align:center;margin-bottom:0px;background-color:lightskyblue">  Soal </h3>
        </div>
          
          <div class="card-body1" style="padding-left:50px;padding-right:50px;padding-bottom:50px;">
            
            <?php
                    //untuk menampilkan kategori umur dan level soal
                    echo ' <p class="card-text text-left" style="font-size:11px">';
                    $res = '';
                    echo '<strong> Kategori Umur : </Strong><br>';
                     foreach($data_kategori_umur as $j){
                       $id_soal_ct = $i->id_soal_ct;
                       $id_soal = $j->id_soal_ct;
                       if($id_soal_ct == $id_soal){
                           $kategori_umur = $j->nama_kategori;
                           $umur_awal = $j->umur_awal;
                           $umur_akhir = $j->umur_akhir;
                           $level_soal = $j->level_soal;
                           $negara_singkatan = $j->negara_singkatan;
                           if($level_soal != 'Umum'){
                           $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / Level '.$level_soal.' / '.$negara_singkatan.')';
                           echo $res.'<br>';
                           }
                           else{
                           $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / '.$level_soal.' / '.$negara_singkatan.')';
                           echo $res.'<br>';
                           } 
                       }
                     }

                   echo '</p>';

                   //untuk menampilkan asal soal / asal bebras
                   echo '<p class="card-text text-left" style="font-size:11px">';
                   $res = '';
                   echo '<strong> Asal Soal : </Strong>';
                   foreach($soal_ct as $j){
                    $id_soal_ct = $i->id_soal_ct;
                    $id_soal = $j->id_soal_ct;
                    $tahun = $j->tahun;
                    if($id_soal_ct == $id_soal){
                        $nama_negara = $j->nama_negara;
                        $res ="Bebras ".$nama_negara." (".$tahun.")";
                    }
                   }
                   echo $res.'</p>';
             ?>
            
            

             <p class="card-text text-left" style="font-size:11px"> <label><strong> Negara Pembuat Soal : <strong></label> <?php echo $data_negara?> </p>
             
             <p class="judul_soal" > <label> Judul Soal : </label> <?php echo $i->judul_soal?></p>
            
              

             <label><strong>Deskripsi Soal : <strong></label> <br>
           <center>
            <?php
                  foreach ($data_imageSoal as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width:auto;height:auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center" style="text-align:center">'.$j->keterangan_gambar.'</p>';
                  } 
                }
              ?>
          </center>
            <br>
            <p class="card-text"><?php echo $i->deskripsi_soal?></p>

            <label><strong>Pertanyaan : <strong></label><p class="card-text"><?php echo $i->pertanyaan?></p>
          <center>
            <?php
                foreach ($data_imagePertanyaan as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width:auto;height:auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center" style="text-align:center">'.$j->keterangan_gambar.'</p>';
                  } 
                }

              ?>
          </center>
          </div>

    <?php  endforeach; ?>


  <?php foreach ($data_pembahasan as $i): ?>

        <div class="card-header">  
          <h3 class="text-center1" style="text-align:center;margin-bottom:0px;background-color:lightskyblue">  Pembahasan </h3>
        </div>

 
      <div class="card-body2"  style="padding:50px">
      <label><strong>Jawaban : <strong></label> 
      <p class="card-text"><?php echo $i->jawaban?></p>
      <center>
        <?php
                foreach ($data_imageJawaban as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width:auto;height:auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center" style="text-align:center">'.$j->keterangan_gambar.'</p>';
                  }
                }
           ?>
      </center>  
      <label><strong>Penjelasan : <strong></label> 

      <p class="card-text"><?php echo $i->penjelasan?></p>
      <center>
      <?php
                foreach ($data_imagePenjelasan as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width:auto;height:auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center" style="text-align:center">'.$j->keterangan_gambar.'</p>';
                  }
                }
              ?>
      </center>
      <label><strong>Hubungan dengan Computational Thinking (CT) : <strong></label> 
      <?php      
                echo '<p style="font-style: italic;">';       
                foreach ($data_konsepCT as $j){    
                  $konsepCT = $j->konsep_ct;
                  echo'<text>'.$konsepCT.'</text><br>';
                } 
                echo '</p>';      
           ?>  
      <p class="card-text"><?php echo $i->penjelasan_ct?></p>
      
      <label><strong>Kata Kunci : <strong></label>
      <?php
        $res='';
                echo '<p>';
                foreach ($data_tag as $j){    
                  $tag = $j->tag;
                  $res = $res.', '.$tag;          
                }
                $res=trim($res,', ');
                echo'<text>'.$res.'</text>';
                echo '<p>';
           ?>

      </div>

<?php  endforeach; ?>