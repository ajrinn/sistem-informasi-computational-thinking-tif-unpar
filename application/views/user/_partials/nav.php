<!-- Navigation -->
<div id="topheader">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">


  <div class="container">
  <img src="<?php echo base_url(); ?>/assets/images/logo.jpg" class="img-fluid" alt="..." style="width:4%;height:4%;margin-right:10px">
    <a class="navbar-brand" href="<?php echo site_url('user/home')?>">Computational Thinking Teknik Informatika UNPAR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
      <ul class="navbar-nav ml-auto navbar-right">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('user/home')?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/konsep_ct')?>">Computational Thinking</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/soal_ct_user')?>">Soal CT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/liputan_ct')?>">Liputan CT TIF UNPAR</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/contact')?>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>


<script>
$( '#topheader .navbar-nav a' ).on( 'click', function () {
	$( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
	$( this ).parent( 'li' ).addClass( 'active' );
});
</script>
