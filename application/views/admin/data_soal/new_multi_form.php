<html>
<?php $this->load->view("admin/_partials/head.php") ?>

<head>
        <!--<title>Multi Step Registration</title>-->
        <style>
            body {
                font-size:14px;
            }

            #addsoal-step {
                font-size:16px;
                margin:auto;
                padding:0;
                width:80%
            }

            #addsoal-step li {
                list-style:none; 
                float:left;
                padding:5px 10px;
                border-top:#0275d8 1px solid;
                border-left:#0275d8 1px solid;
                border-right:#0275d8 1px solid;
                border-radius:5px 5px 0 0;
            }

            .active {
                color:#FFF;
            }

            #addsoal-step li.active {
                background-color:#0275d8;
            }

            #soalpembahasan-form {
                clear:both;
                border:1px #0275d8 solid;
                padding:20px;
                width:80%;
                margin:auto;
            }

            .demoInputBox {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: 80%;
            }

            .keteranganGambar {
                padding: 3px 5px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: 80%;
            }

            .datasoalpembahasan-error {
                color:#FF0000; 
                padding-left:10px;
            }

            .message {
                color: #00FF00;
                font-weight: bold;
                width: 100%;
                padding: 10;
            }

            .btnAction {
                padding: 5px 10px;
                border: 0;
                color: #FFF;
                cursor: pointer;
                margin-top:15px;
                margin-bottom:25px;
            }

            label {
                line-height:15px;
            }

            .form-check label {
                line-height:24px ; 
            }

            .kategori_level {
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: 100%;
            }
            
            .tipe_gambar{
                padding: 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }

            .tipe_gambarPembahasan{
                padding: 8px;
                margin-top : 10px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
            }

            .alert-info{
                margin-left:0px;
                margin-right:10px;
            }

            .form-check{
              
            }

        </style>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

               <?php  if(isset($success)):?>
				<div class="alert alert-success" role="alert">
                   <a href="#" class="close" data-dismiss="alert">&times;</a>
				   <?php echo $this->session->flashdata('success'); ?>
				</div>
                <?php endif; ?>
                
                <?php if ($this->session->flashdata('gagal_tipe_file_salah')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_tipe_file_salah'); ?>
				</div>
                <?php elseif ($this->session->flashdata('gagal_file_terlalu_besar')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_file_terlalu_besar'); ?>
				</div>
				<?php endif; ?>

				
				<br>
				<h5 class="font-weight-bold text-center">Tambah Soal CT dan Pembahasan</h5>
				<br>

			    <div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/soal_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>

                 <div class="card-body">

                    <ul id="addsoal-step">
                        <li id="dataSoal" class="active"> Data Soal CT</li> 
                        <li id="dataPembahasan"> Data Pembahasan Soal CT </li>
                    </ul>

                 <?php 
                   
                    $attributes = array ('name' => 'frmSoalPembahasanCT','id' => 'soalpembahasan-form');
                    echo form_open_multipart('admin/soal_ct/add',$attributes);
                 ?>

                 <div id="dataSoal-field">
                    
                    <label>Judul Soal*</label><span id="judul_soal-error" class="datasoalpembahasan-error"></span>
                    <div><input type="text" name="judul_soal" id="judul_soal" class="demoInputBox"/></div>
                    <br>
                    <label>Tipe Soal*</label>
                    <div >
                         <select name="tipe_soal" id="tipe_soal" class="demoInputBox" class="dropdown-menu">
                            <option class="dropdown-item" value="Pilihan Ganda">Pilihan Ganda</option>
                            <option class="dropdown-item" value="Jawaban Singkat">Jawaban Singkat</option>                                                                         
                         </select>
                     </div>
                     <br>
                    <label>Deskripsi Soal*</label><span id="deskripsi_soal-error" class="datasoalpembahasan-error"></span>
                    <div><textarea type="text" name="deskripsi_soal" id="deskripsi_soal" class="demoInputBox"></textarea>
                               <script>
                                        CKEDITOR.replace( 'deskripsi_soal',{
														   enterMode: CKEDITOR.ENTER_BR
													    });
            				  </script>
                    </div>
                    <br>  

                    <label>Pertanyaan*</label><span id="pertanyaan-error" class="datasoalpembahasan-error"></span>
                    <div><textarea type="text" name="pertanyaan" id="pertanyaan" class="demoInputBox"/></textarea>
                                <script>
                                        CKEDITOR.replace( 'pertanyaan',{
														   enterMode: CKEDITOR.ENTER_BR
													    });
            				  </script>
                    </div>
                    <br>

                    <label>Tahun Soal*</label>
                    <div>
                         <select name="tahun" id="tahun" class="demoInputBox">
                            <?php
                            
                                $max = 2019;
                                for($i = 2004 ; $i <= $max ; $i++ ) {
                                        echo '<option value='.$i.'>'.$i.'</option>';
                                }
                            ?> 
                         </select>
                     </div>
                     <br>

                     <label>Negara Asal Soal*</label>
                    <div>
                         <select name="negara_asal_soal" id="negara_asal_soal" class="demoInputBox">
                            <?php
                                foreach ($negara_pembuat_soal as $i) {
                                        echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
                                    }
                            ?> 
                         </select>
                     </div>
                     <br>

                    <label>Negara Pembuat Soal*</label>
                    <div>
                         <select name="negara_pembuat_soal" id="negara_pembuat_soal" class="demoInputBox">
                            <?php
                                foreach ($negara_pembuat_soal as $i) {
                                        echo '<option value='.$i->id_negara.'>'.$i->nama_negara.'</option>';
                                    }
                            ?> 
                         </select>
                     </div>
                     <br>


                     <label for="konsepCT[]">Konsep Computational Thinking*</label><span id="konsepCT-error" class="datasoalpembahasan-error"></span>
                    
                    <?php foreach ($konsep_ct as $j) :?>
                     <div class="form-check">
  					    <input class="form-check-input" type="checkbox" name="konsepCT[]" value="<?php echo $j -> id_konsep_ct?>" id="konsep<?php echo $j -> id_konsep_ct?>">
  					    <label class="form-check-label" for="konsep<?php echo $j -> id_konsep_ct?>"> <?php echo $j -> konsep_ct?> </label>
					</div>
                    <?php endforeach;?>

                    <div style="margin-top:15px">
                      <a href="<?php echo base_url('index.php/admin/konsep_ct/add'); ?>" class="btn btn-success" role="button" aria-pressed="true"> <i class="fas fa-plus"></i> Tambah Konsep CT</a>
                    </div>
                    
                    <br>
                    <label for="tagCT[]">Tags (Kata Kunci)*</label><span id="tagCT-error" class="datasoalpembahasan-error"></span>           				
                    
                    <div class="row">
                      <div class="col">

                        <?php 
                           	$batas1 = ceil($jumlah_tag/4); 
                               $count = 1;		
                               foreach ($tag_ct as $i): 
                                   if($count>=1 && $count<=$batas1) : 
                        ?>
                        <div class="form-check">
  					        <input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>">
  					        <label class="form-check-label" style = "font-size:12px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    </div>
                        <?php   $count++;  endif; endforeach; 	?>

        
                       </div>
                      <div class="col">
                        
                      <?php 
                            $batas1 = ceil($jumlah_tag/4); 
                            $count = $batas1+1;		
                            foreach (array_slice($tag_ct, $batas1)  as $i): 
                                if($count>$batas1 && $count<=2*$batas1) : 
                                    //echo $count;	
                        ?>
                        <div class="form-check">
  					        <input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>">
  					        <label class="form-check-label" style = "font-size:12px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    </div>
                        <?php   $count++;  endif; endforeach; 	?>

                       </div>
                       <div class="col">                         
                       <?php 
                            $batas1 = ceil($jumlah_tag/4); 
                            $count = (2*$batas1)+1;		
                            foreach (array_slice($tag_ct, (2*$batas1))  as $i): 
                                if($count>2*$batas1 && $count<=3*$batas1) : 
                                    //echo $count;	
                        ?>
                        <div class="form-check">
  					        <input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>">
  					        <label class="form-check-label" style = "font-size:12px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    </div>
                        <?php   $count++;  endif; endforeach; 	?>
                        
                        </div>
                        <div class="col">
                                
                        <?php 
                            $batas1 = ceil($jumlah_tag/4); 
                            $count = (3*$batas1)+1;		
                            foreach (array_slice($tag_ct, (3*$batas1))  as $i): 
                                if($count>3*$batas1 && $count<=4*$batas1) : 
                                    //echo $count;	
                        ?>
                        <div class="form-check">
  					        <input class="form-check-input" type="checkbox" name="tagCT[]" value="<?php echo $i -> id_tag?>" id="tag<?php echo $i -> id_tag?>">
  					        <label class="form-check-label" style = "font-size:12px" for="tag<?php echo $i -> id_tag?>"> <?php echo $i -> tag?> </label>
					    </div>
                        <?php   $count++;  endif; endforeach; 	?>

                        </div>

                    </div>
                
                    <div style="margin-top:15px">
                      <a href="<?php echo base_url('index.php/admin/tag_ct/add'); ?>" class="btn btn-success" role="button" aria-pressed="true"> <i class="fas fa-plus"></i> Tambah Tag (Kata Kunci)</a>
                    </div>

                    <br>
							<label>Kategori Umur dan Level Soal*</label>
                    		<div class="alert alert-info row" >             
                                <div class="col">
                                <label>Kategori Umur</label> <br>
                        		 	<select name="kategori_umur[]" id="kategori_umur" class="kategori_level" class="dropdown-menu">  
                                         <?php foreach ($kategori_umur as $i): ?> 
                                         <option class="dropdown-item" value="<?php echo $i->id_kategori_umur?>"> <?php echo $i->nama_kategori?> / <?php echo $i->negara_singkatan?> (<?php echo $i->umur_awal?> - <?php echo $i->umur_akhir?> tahun) </option>
                                         <?php endforeach; ?>                                       
                                         </option>                                                                             
                         			</select>  
                                </div>
                                
                                <div class="col">
                                <label>Level Soal</label> <br>
                                     <select name="level_soal[]" id="level_soal" class="kategori_level" class="dropdown-menu" style="width:auto">
                                         <?php foreach ($level_soal as $i): ?> 
                                         <option class="dropdown-item" value="<?php echo $i->id_level_soal?>"> <?php echo $i->level_soal?> (<?php echo $i->keterangan?>) </option>
                                         <?php endforeach; ?>                                                                        
                         			</select> 
                                </div>
                     		</div>

                             <div id="uploadFileContainer"></div>
                             <div>
                                    <button id="ADDFILE" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Kategori Umur dan Level Soal </button>    
                            </div>
                    
                     <br>
                            <label>Upload Gambar Soal</label>
                    		<div class="alert alert-info row" >             
                                <div class="col">
                                    <label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>
                                    <input type="file" accept="image/*" id="multipleFiles" name="multipleFiles[]" class="image_soal">
                                </div>
                                
                                <div class="col">
                                <label>Tipe Gambar</label> <br>
                                     <select name="tipe_gambar[]" id="tipe_gambar" class="tipe_gambar" class="dropdown-menu" style="width:auto">
                                         <option class="dropdown-item" value="Soal (Deskripsi Soal)"> Soal (Deskripsi Soal)</option>  
                                         <option class="dropdown-item" value="Soal (Pertanyaan)"> Soal (Pertanyaan)</option>                                                                           
                         			</select> 
                                </div>
                                
                                <div style="margin-top:5px">
                                    <label style="margin-left:17px">Keterangan Gambar : </label> <br>
                                    <input type="text" name="keteranganGambar[]" style="margin-left:17px;width:300px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>      
                                </div>
                                
                     		</div>

                             <div id="uploadFileContainer2"></div>
                                 <div>
                                    <button id="ADDFILE2" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar Soal</button>    
                                </div>
                             </div>


                    

                 <div id="dataPembahasan-field" style="display:none">
                    <label>Jawaban*</label><span id="jawaban-error" class="datasoalpembahasan-error"></span>
                    <div><textarea type="text" name="jawaban" id="jawaban" class="demoInputBox"></textarea>
                              <script>
                                        CKEDITOR.replace( 'jawaban',{
														   enterMode: CKEDITOR.ENTER_BR
													    });
            				  </script>
                    </div>
                    <br>
                    <label>Penjelasan*</label><span id="penjelasan-error" class="datasoalpembahasan-error"> </span>
                    <div><textarea type="text" name="penjelasan" id="penjelasan" class="demoInputBox"/></textarea>
                             <script>
                                        CKEDITOR.replace( 'penjelasan',{
														   enterMode: CKEDITOR.ENTER_BR
													    });
            				 </script>
                    </div>
                    <br>
                    <label>Hubungan dengan Computational Thinking (CT)*</label><span id="penjelasan_ct-error" class="datasoalpembahasan-error"> </span>
                    <div><textarea type="text" name="penjelasan_ct" id="penjelasan_ct" class="demoInputBox"/></textarea>
                             <script>
                                        CKEDITOR.replace( 'penjelasan_ct',{
														   enterMode: CKEDITOR.ENTER_BR
													    });
            				 </script>
                    
                    </div>
                    <br>

                    <label>Upload Gambar Pembahasan</label>
                    		<div class="alert alert-info row" >             
                                <div class="col image_soal">
                                    <label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>
                                    <input type="file" accept="image/*" name="multiplePembahasan[]" id="multiplePembahasan" >
                                </div>
                                
                                <div class="col">
                                <label>Tipe Gambar</label> <br>
                                     <select name="tipe_gambarPembahasan[]" id="tipe_gambarPembahasan" class="tipe_gambar" class="dropdown-menu" style="width:auto"> 
                                         <option class="dropdown-item" value="Pembahasan (Jawaban)"> Pembahasan (Jawaban)</option> 
                                         <option class="dropdown-item" value="Pembahasan (Penjelasan Jawaban)"> Pembahasan (Penjelasan Jawaban)</option>                                                                            
                         			</select> 
                                </div>
                                <br>
                                
                                <div style="margin-top:5px">
                                <label style="margin-left:17px">Keterangan Gambar : </label> <br>
                                <input type="text" name="keteranganGambarPembahasan[]" style="margin-left:17px;width:500px" id="keteranganGambarPembahasan" class="keteranganGambar" placeholder="Keterangan Gambar"/>
                                </div>
                            </div>

                             <div id="uploadFileContainer3"></div>
                                 <div>
                                    <button id="ADDFILE3" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar Pembahasan</button>    
                                </div>
                             </div>
                 </div>

                 

                 <div align="center">
                    
                    <button class="btnAction btn btn-primary" type="button" name="back" id="back" value="Back" style="display:none;">
                    <i class="fas fa-arrow-left"></i> Back
                    </button>
                     <button class="btnAction btn btn-primary" type="button" name="next" id="next" value="Next"> 
                        Next <i class="fas fa-arrow-right"></i>
                    </button>
                    <input class="btnAction btn btn-primary" type="submit" name="finish" id="finish" value="Finish" style="display:none;">
                 </div>

                 

                  <?php echo form_close(); ?>


              </div>

              <div class="card-footer small text-muted">
						* required fields
			 </div>

            

          </div>
              
              
               
             </div>

                

                </div>
             <div>
         </div>

           
	           </div><!-- /.container-fluid -->
                        <?php //$this->load->view("admin/_partials/footer.php") ?> <!-- Sticky Footer -->
	        </div><!-- /.content-wrapper --> 
       </div><!-- /#wrapper -->



      <!--
      <img class="card-img-top" src="<?php //echo base_url();?>index.php/admin/Upload_Gambar/display_gambar/1" alt="Card image cap"> <br>
      <a class="card-img-top" href="<?php //echo site_url('admin/Upload_Gambar/display_gambar/1') ?>" > Gambar 1 <a> <br>
      <a class="card-img-top" href="<?php //echo site_url('admin/Upload_Gambar/display_gambar/2') ?>" > Gambar 2 <a>
      -->
        
		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
        <?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>
    function validate(){
        var output = true;
        $(".datasoal-error").html('');

        if($("#dataSoal-field").css('display') != 'none'){
            if(!($("#judul_soal").val())){
                output = false;
                $("#judul_soal-error").html("(Judul soal required!)");
            }

            if(!($("#tipe_soal").val())){
                output = false;
                $("#tipe_soal-error").html("(Tipe soal required!)");
            }

            var deskripsi_soal = CKEDITOR.instances.deskripsi_soal.getData();
            if(deskripsi_soal == ''){
                output = false;
                $("#deskripsi_soal-error").html("(Deskripsi soal required!)");
            }

            var pertanyaan = CKEDITOR.instances.pertanyaan.getData();
            if(pertanyaan == ''){
                output = false;
                $("#pertanyaan-error").html("(Pertanyaan required!)");
            }

            if(!($("#negara_pembuat_soal").val())){
                output = false;
                $("#negara_pembuat_soal-error").html("(Negara Pembuat Soal required!)");
            }

            if (!$("#konsep1").is(":checked") && !$("#konsep2").is(":checked") && !$("#konsep3").is(":checked") && !$("#konsep4").is(":checked") && !$("#konsep5").is(":checked") && !$("#konsep6").is(":checked")) {
                output = false;
                $("#konsepCT-error").html("(Konsep CT required!)");
            }

            if(!$("input[name='tagCT[]']").is(":checked")){
                output = false;
                $("#tagCT-error").html("(Tag required!)");
            }

        }
        
        if($("#dataPembahasan-field").css("display") != 'none'){
            
            var jawaban = CKEDITOR.instances.jawaban.getData();
            if(jawaban == ''){
                output = false;
                $("#jawaban-error").html("(Jawaban required!)");
            }
            
            var penjelasan = CKEDITOR.instances.penjelasan.getData();
            if(penjelasan == ''){
                output = false;
                $("#penjelasan-error").html("(Penjelasan required!)");
            }

            var penjelasan_ct = CKEDITOR.instances.penjelasan_ct.getData();
            if(penjelasan_ct == ''){
                output = false;
                $("#penjelasan_ct-error").html("(Hubungan dengan Computational Thinking (CT) required!)");
            }

        }


        return output;
    }

    $(document).ready(function(){
        $("#next").click(function(){
            var output = validate();
            if(output === true){
                var current = $(".active");
                var next = $(".active").next("li");
                if(next.length > 0){
                    $("#" + current.attr("id") + "-field").hide();
                    $("#" + next.attr("id") + "-field").show();
                    $("#back").show();
                    $("#next").hide();
                    $("#finish").show();
                    $(".active").removeClass("active");
                    next.addClass("active");
                   // if($(".active").attr("id") == $("li").last().attr("id")){
                   //     $("#next").hide();
                   //     $("#finish").show();
                   // }
                }
            }     
        });

        $("#back").click(function (){
            var current = $(".active");
            var prev = $(".active").prev("li");
            if(prev.length > 0){
                $("#" + current.attr("id") + "-field").hide();
                $("#" + prev.attr("id") + "-field").show();
                $("#next").show();
                $("#finish").hide();
                $("#back").hide();
                $(".active").removeClass("active");
                prev.addClass("active");
            }
        });

        
        $("input#finish").click(function (e) {
            var output = validate ();
            var current = $(".active");

            if(output === true){
                return true;
            }
            else{
                //mencegah refresh
                e.preventDefault();
                $("#" + current.attr("id") + "-field").show();
                $("#back").show();
                $("#finish").show();
            }
        });


        $("input#finish").click(function (e) {
            var output = validate ();
            var current = $(".active");

            if(output === true){
                return true;
            }
            else{
                //mencegah refresh
                e.preventDefault();
                $("#" + current.attr("id") + "-field").show();
                $("#back").show();
                $("#finish").show();
            }
        }); 

        $(document).on('click','button#ADDFILE', function(event){
				event.preventDefault();
				addFileInput();
		});                 

		function addFileInput() {
			var html = '';
            html+='<div class="alert alert-info row" >';            
            html+='<div class="col">';
            html+='<label>Kategori Umur</label> <br>';
            html+='<select name="kategori_umur[]" id="kategori_umur" class="kategori_level" class="dropdown-menu">';
            html+='<?php foreach ($kategori_umur as $i): ?>'; 
            html+='<option class="dropdown-item" value="<?php echo $i->id_kategori_umur?>"> <?php echo $i->nama_kategori?> / <?php echo $i->negara_singkatan?>  (<?php echo $i->umur_awal?> - <?php echo $i->umur_akhir?> tahun) </option>';
            html+='<?php endforeach; ?>';                                           
            html+='</option>';                                                                             
            html+='</select>'; 
            html+='</div>'; 
            html+='<div class="col">';
            html+='<label>Level Soal</label> <br>';
            html+='<select name="level_soal[]" id="level_soal" class="kategori_level" class="dropdown-menu" style="width:auto">';
            html+='<?php foreach ($level_soal as $i): ?>'; 
            html+='<option class="dropdown-item" value="<?php echo $i->id_level_soal?>"> <?php echo $i->level_soal?> (<?php echo $i->keterangan?>) </option>';
            html+='<?php endforeach; ?>';                                                                             
            html+='</select>'; 
            html+='</div>';
            html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'; 
            html+='</div>';
			$("div#uploadFileContainer").append(html);
		}
        
        $(document).on('click','button#ADDFILE2', function(event){
				event.preventDefault();
				addFileInput2();
		});                 

		function addFileInput2() {
			var html = '';
            html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>';
            html+='<input type="file" accept="image/*" name="multipleFiles[]" class="image_soal">';
            html+='</div>';   
            html+='<div class="col">';
            html+='<label>Tipe Gambar</label> <br>';
            html+='<select name="tipe_gambar[]" id="tipe_gambar" class="tipe_gambar" class="dropdown-menu" style="width:auto">';
            html+='<option class="dropdown-item" value="Soal (Deskripsi Soal)"> Soal (Deskripsi Soal)</option>';  
            html+='<option class="dropdown-item" value="Soal (Pertanyaan)"> Soal (Pertanyaan)</option>';                                                                            
            html+='</select>'; 
            html+='</div>';
            html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            html+='<div style="margin-top:5px">';
            html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
            html+='<input type="text" name="keteranganGambar[]" style="margin-left:17px;width:300px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
            html+='</div>';
            html+='</div>';
			$("div#uploadFileContainer2").append(html);
		}

        $(document).on('click','button#ADDFILE3', function(event){
				event.preventDefault();
				addFileInput3();
		});                 

		function addFileInput3() {
			var html = '';
            html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>';
            html+='<input type="file" accept="image/*" name="multiplePembahasan[]" >';
            html+='</div>';   
            html+='<div class="col">';
            html+='<label>Tipe Gambar</label> <br>';
            html+='<select name="tipe_gambarPembahasan[]" id="tipe_gambar_pembahasan" class="tipe_gambarPembahasan" class="dropdown-menu" style="width:auto">';
            html+='<option class="dropdown-item" value="Pembahasan (Jawaban)"> Pembahasan (Jawaban)</option>'; 
            html+='<option class="dropdown-item" value="Pembahasan (Penjelasan Jawaban)"> Pembahasan (Penjelasan Jawaban)</option>';                                                                            
            html+='</select>'; 
            html+='</div>';
            html+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            html+='<div style="margin-top:5px">';
            html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
            html+='<input type="text" name="keteranganGambarPembahasan[]" style="margin-left:17px;width:300px" id="keteranganGambarPembahasan" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
            html+='</div>';
            html+='</div>';
			$("div#uploadFileContainer3").append(html);
		}

        $(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
        
    
    
    });

    

</script>



</html>