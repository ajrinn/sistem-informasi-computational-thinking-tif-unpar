<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>

	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<br>
					<h5 class="text-center text-bold font-weight-bold">Daftar Level Soal CT</h5>
				<br>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/level_soal_ct/add')?>"><i class="fas fa-plus"></i> Tambah Level Soal CT</a>
					</div>
					<div class="card-body">

					<?php if (!empty($data_level)): ?>

						<div class="table-responsive">
							<table class="table table-bordered table-hover" id="table_level" width="100%" cellspacing="0">
								
							
								<thead>
									<tr>
										<th>No</th>
										<th>Level Soal</th>
										<th>Keterangan</th>
									</tr>
								</thead>

								<tbody>

								<?php
							    $num=1; 
								 foreach ($data_level as $i): 
								 ?>
									
									<tr>
										<td>
											<?php 
												 if(($page > $num)){
													$num = $page+1;													 
												 }
												 echo $num;											
											?>
										</td>
										<td>
											<?php 
											     $level_soal = htmlspecialchars( $i->level_soal);
												 echo $level_soal;
											 ?>
										</td>
										<td>
											<?php 
											     $keterangan = htmlspecialchars( $i->keterangan);
												 echo $keterangan;
											 ?>
										</td>
										<td width="250">
											<a href="<?php echo site_url('admin/level_soal_ct/edit/'.$i->id_level_soal) ?>"
								            class="btn btn-small text-warning"><i class="fas fa-edit"></i> Sunting</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/level_soal_ct/delete/'.$i->id_level_soal) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>

								<?php 
									 $num++;
									  endforeach; 
								?>

								</tbody>
							</table>
						</div>

						<div class="row">
							<div class="col">
							<?php echo $pagination;?>
							</div>
						</div>

						<?php endif; ?>
						
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
		$('#btn-delete').attr('href',url);
		$('#deleteModal').modal();
		}

		$(document).ready(function(){

			
			$('#table_level').DataTable({
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 3 ],
					orderData: [ 3, 0 ]
				} ]
			});

			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});

	</script>

</body>

</html>