<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ImagePembahasanCT_model extends CI_Model{

    private $_table = "image_pembahasan";

    function insert_image_pembahasan ($id_pembahasan,$id_image){
        $data = array('id_pembahasan' => $id_pembahasan, 'id_image' => $id_image);
        $result = $this->db->insert($this->_table,$data);
        if($result !== NULL){
            return TRUE;
        }
        return FALSE;
    }

}