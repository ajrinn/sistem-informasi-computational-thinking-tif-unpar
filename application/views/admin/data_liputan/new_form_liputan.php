<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
    <style>
         .alert-info{
                margin-left:0px;
                margin-right:10px;
            }

            .keteranganGambar {
                padding: 3px 5px;
                border: #CDCDCD 1px solid;
                border-radius: 4px;
                background-color: #FFF;
                width: 80%;
            }
    </style>

</head>

<?php //echo form_open_multipart('admin/Soal_CT');?>



<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<!--breadcrumbs disini-->

				<!--Bagian session-->
				<?php if ($this->session->flashdata('berhasil_tambah_gambar')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('berhasil_tambah_gambar'); ?>
				</div>
				<?php elseif ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php elseif ($this->session->flashdata('gagal_tipe_file_salah')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_tipe_file_salah'); ?>
				</div>
                <?php elseif ($this->session->flashdata('gagal_file_terlalu_besar')): ?>
				<div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo $this->session->flashdata('gagal_file_terlalu_besar'); ?>
				</div>
				<?php endif; ?>
				
				<br>
				<h5 class="font-weight-bold text-center">Tambah Liputan CT</h5>
				<br>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/liputan_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/liputan_ct/add') ?>" method="post" name="form1" enctype="multipart/form-data">

                            <div class="form-group">
								<label for="judul_liputan">Judul Liputan*</label>
								<input class="form-control 
								<?php echo form_error('judul_liputan') ? 'is-invalid':'' ?>"
								 type="text" name="judul_liputan" placeholder="Judul Liputan"/>
								<div class="invalid-feedback">
									<?php echo form_error('judul_liputan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="deskripsi_liputan">Deskripsi Liputan*</label>
								<textarea class="form-control 
								<?php echo form_error('deskripsi_liputan') ? 'is-invalid':'' ?>"
								 type="text" name="deskripsi_liputan" placeholder="Deskripsi Liputan" />
								 </textarea>
                                 <script type="text/javascript">
                				   // Replace the <textarea id="editor1"> with a CKEditor
                				   // instance, using default configuration.

													 CKEDITOR.replace( 'deskripsi_liputan',{
														enterMode: CKEDITOR.ENTER_BR
													 });

   								</script>
								<div class="invalid-feedback">
									<?php echo form_error('deskripsi_liputan') ?>
								</div>
							</div>

							<div class="form-group">
							<label for="tanggal">Tanggal*</label>
						    <input class="form-control 
							<?php echo form_error('tanggal') ? 'is-invalid':'' ?>" type="date" name="tanggal" />
							<div class="invalid-feedback">
								<?php echo form_error('tanggal') ?>
							</div>
							</div>

                            <br>
                            <label>Upload Gambar Soal</label>
                    		<div class="alert alert-info row" >   

                                <div class="col">
                                    <label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>
                                    <input type="file" accept="image/*" id="multipleFiles" name="multipleFiles[]" class="image_soal">
                                </div>
                                
                                <div style="margin-top:5px">
                                    <label style="margin-left:17px">Keterangan Gambar : </label> <br>
                                    <input type="text" name="keteranganGambar[]" style="margin-left:17px;width:500px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>      
                                </div>
                                
                     		</div>

                             <div id="uploadFileContainer2"></div>
                                 <div>
                                    <button id="ADDFILE2" class="btn btn-success"> <i class="fas fa-plus"></i> Tambah Gambar</button>    
                                </div>
                             </div>


					
					<div class="" style="width:5%;margin:0 auto;padding-bottom:20px" >
					 <input class="btn btn-success" id="myButton" type="submit" name="submit" value="Save" style="margin-top:20px">
					</div>

			</form>

			</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->
			

				<!-- Sticky Footer -->
				<?php //$this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	   <?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

<script>

$(document).on('click','button#ADDFILE2', function(event){
				event.preventDefault();
				addFileInput2();
		});                 

		function addFileInput2() {
			var html = '';
            html+='<div class="alert alert-info row" >';             
            html+='<div class="col">';
            html+='<label><strong>Pilih Gambar </strong>(Ukuran maksimum 2 MB)</label>';
            html+='<input type="file" accept="image/*" name="multipleFiles[]" class="image_soal">';
            html+='</div>';   
            html+='<div style="margin-top:5px">';
            html+='<label style="margin-left:17px">Keterangan Gambar : </label> <br>';
            html+='<input type="text" name="keteranganGambar[]" style="margin-left:17px;width:500px" id="keteranganGambar" class="keteranganGambar" placeholder="Keterangan Gambar"/>';
            html+='</div>';
            html+='<div><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
            html+='</div>';
			$("div#uploadFileContainer2").append(html);
		}

		$(document).ready(function(){
			$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			});
		});
</script>


</html>


