<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {

    private $_table = "admin";
    private $id_admin;
    private $nama;
    private $username;
    private $password;

    public function rules(){

        return [
            [ 'field' => 'nama',
              'label' =>  'Nama',
              'rules' =>  'required'],
            
            [ 'field' => 'username',
              'label' =>  'Username',
              'rules' =>  'required'],
            
              [ 'field' => 'password',
              'label' =>  'Password',
              'rules' =>  'required'], 
        ];
    }

    public function getAll(){
       return $this->db->get($this->_table)->result();
       //Artinya seperti select * from admin
       //method yang mengembalilan array berisi objek dari row tabel
    }
    
    public function getByID($id){
        return $this->db->get_where($this->_table,["id_admin"=>$id])->row();
        //Artinya seperti select * from admin where admin_id = $id
        //method yang mengembalikan sebuah objek dari row tabel
    }

    public function save(){
        //method ini menyimpan data ke tabel admin
        $post = $this->input->post(); //mengambil data dari form
        $this->nama = $post["nama"]; //isi field nama
        $this->username = $post ["username"]; // isi field username
        $this->password =  md5($post["password"]);  //isi field password

        $this->db->query('alter table master_tags AUTO_INCREMENT=0');

        $data_admin = $this->getAll();
        $cek = true;

        foreach($data_admin as $i){
            $nama_temp = $i->nama;
            $username_temp = $i->username;
            $password_temp = $i->password;
            
            if($this->nama == $nama_temp || $this->username == $username_temp || $this->password == $password_temp){
              $cek = false;
              break;
            }
    
          }
        
        
        if($cek == true){
            $this->db->query("
                                INSERT INTO admin (nama,username,password) values ('$this->nama','$this->username','$this->password');
            ");
            return true;
        }
        else{
            return false;
        }

    }

    public function update(){
        //method ini mengupdate data tabel admin
        
        $post = $this->input->post();//mengambil input yang dikirim dari from
        $this->id_admin = $post["id"];
        $this->nama = $post["nama"];
        $this->username = $post["username"];
        $this->password = md5($post["password"]);
        //$this->db->update($this->_table,$this, array('id_admin' => $post['id']));

        $data_admin = $this->getAll();
        $cek = true;

        foreach($data_admin as $i){
            $id_temp = $i->id_admin;
            $nama_temp = $i->nama;
            $username_temp = $i->username;
            $password_temp = $i->password;
            
            if($this->id_admin != $id_temp && ($this->nama == $nama_temp || $this->username == $username_temp || $this->password == $password_temp)){
              $cek = false;
              break;
            }

        }

        if($cek == true){
            $this->db->query("
                                UPDATE admin
                                SET admin.nama = '$this->nama',
                                    admin.username = '$this->username',
                                    admin.password =  '$this->password'
                                WHERE admin.id_admin = '$this->id_admin';
            ");
            return true;
        }
        else{
            return false;
        }
        
    }

    public function delete($id){
        //method ini menghapus data tabel admin
        return $this->db->delete($this->_table,array("id_admin" => $id));
    }



    public function cek_login($username,$password){
        $periksa = $this->db->get_where('admin',array('username'=>$username,'password'=>md5($password)));
        if($periksa->num_rows()>0){
            return 1;
        }
        else{
            return 0;
        }     
    }

    public function jumlah_data_admin(){
		return $this->db->get('admin')->num_rows();
    }

    public function get_data_admin($number,$offset){
   //   $this->db->order_by("jumlah_konsep desc");
      return $query = $this->db->get('admin',$number,$offset)->result();		
    }


    

}