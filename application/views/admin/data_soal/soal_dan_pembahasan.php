<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("user/_partials/head.php") ?>
</head>

<body>

   
   <?php foreach ($data_soal as $i): ?>
    <div class="card" style="padding-bottom:30px">
          
        <div class="card-header">
		   <a href="<?php echo site_url('admin/soal_ct/')?>"><i class="fas fa-arrow-left"></i> Back</a>
		</div>

          <div class="card-header">
            <h5 class="text-center">Soal</h5>
          </div>
          
          <div class="card-body" style="padding-left:50px;padding-right:50px">

          <?php
                    //untuk menampilkan kategori umur dan level soal
                    echo ' <p class="card-text text-left" style="font-size:11px">';
                    $res = '';
                    echo '<strong> Kategori Umur : </Strong><br>';
                     foreach($data_kategori_umur as $j){
                       $id_soal_ct = $i->id_soal_ct;
                       $id_soal = $j->id_soal_ct;
                       if($id_soal_ct == $id_soal){
                           $kategori_umur = $j->nama_kategori;
                           $umur_awal = $j->umur_awal;
                           $umur_akhir = $j->umur_akhir;
                           $level_soal = $j->level_soal;
                           $negara_singkatan = $j->negara_singkatan;
                           if($level_soal != 'Umum'){
                           $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / Level '.$level_soal.' / '.$negara_singkatan.')';
                           echo $res.'<br>';
                           }
                           else{
                           $res = $kategori_umur.' ('.$umur_awal.' - '.$umur_akhir.' tahun / '.$level_soal.' / '.$negara_singkatan.')';
                           echo $res.'<br>';
                           } 

                       }
                     }

                   echo '</p>';

                    //untuk menampilkan asal soal / asal bebras
                    echo '<p class="card-text text-left" style="font-size:11px">';
                    $res = '';
                    echo '<strong> Asal Soal : </Strong>';
                    foreach($soal_ct as $j){
                     $id_soal_ct = $i->id_soal_ct;
                     $id_soal = $j->id_soal_ct;
                     $tahun = $j->tahun;
                     if($id_soal_ct == $id_soal){
                         $nama_negara = $j->nama_negara;
                         $res ="Bebras ".$nama_negara." (".$tahun.")";
                     }
                    }
                    echo $res.'</p>';
             ?>
             
             <p class="card-text text-left" style="font-size:11px"> <label>Negara Pembuat Soal : </label> <?php echo $data_negara?> </p>

            <p class="card-text text-center"><label>Judul : </label> <?php echo $i->judul_soal?></p>

            <label>Deskripsi Soal : </label>
            <?php
                  foreach ($data_imageSoal as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width: auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center">'.$j->keterangan_gambar.'</p>';
                  } 
                }
              ?>

            <p class="card-text"><?php echo $i->deskripsi_soal?></p>


            <label>Pertanyaan : </label><p class="card-text"><?php echo $i->pertanyaan?></p>
            
            <?php
                foreach ($data_imagePertanyaan as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width: auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center">'.$j->keterangan_gambar.'</p>';
                  }
                }

              ?>

          </div>

    </div>
    <?php  endforeach; ?>


  <?php foreach ($data_pembahasan as $i): ?>
  <div class="card" style="padding-bottom:30px">

    <div class="card-header">
            <h5 class="text-center">Pembahasan</h5>
    </div>

    <div class="card-body"  style="padding:50px">
      <label>Jawaban : </label>
      <?php

               foreach ($data_imageJawaban as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width: auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center">'.$j->keterangan_gambar.'</p>';
                  }
                }
              ?>
       <p class="card-title"><?php echo $i->jawaban?></p>

      <label>Penjelasan : </label> <p class="card-text">
      <?php echo $i->penjelasan?></p>
      <?php
                foreach ($data_imagePenjelasan as $j){
                  if($j->tipe_gambar != null){
                    echo '<img class="card-img img-responsive center-block" style="width: auto;" src="data:image/jpeg;base64,'.$this->controller->display_gambar($j->id_image).'"/>';
                    echo '<p class="text-center">'.$j->keterangan_gambar.'</p>';
                  }
                }
              ?>
      

      <label>Hubungan dengan Computational Thinking : </label> 
      <?php      
                echo '<p style="font-style: italic;">';       
                foreach ($data_konsepCT as $j){    
                  $konsepCT = $j->konsep_ct;
                  echo'<text>'.$konsepCT.'</text><br>';
                } 
                echo '</p>';      
           ?>
      <p class="card-text"><?php echo $i->penjelasan_ct?></p>

      <label>Kata Kunci : </label>
      <?php
        $res='';
                echo '<p>';
                foreach ($data_tag as $j){    
                  $tag = $j->tag;
                  $res = $res.', '.$tag;          
                }
                $res=trim($res,', ');
                echo'<text>'.$res.'</text>';
                echo '<p>';
           ?>
          
    </div>

   </div>
    <?php  endforeach; ?>

</body>


